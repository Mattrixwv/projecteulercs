﻿//ProjectEuler/ProjectEulerCS/src/ProblemSelection.cs
//Matthew Ellison
// Created: 08-21-20
//Modified: 06-05-21
//This class holds all of the functions needed to handle a problem
/*
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


using System.Collections.Generic;

using ProjectEulerCS.Problems;


namespace ProjectEulerCS{
	public class ProblemSelection{
		//Holds the valid problem numbers
		private static readonly List<int> _PROBLEM_NUMBERS = new List<int>()
				{ 0,  1,  2,  3,  4,  5,  6,  7,  8,  9,
				 10, 11, 12, 13, 14, 15, 16, 17, 18, 19,
				 20, 21, 22, 23, 24, 25, 26, 27, 28, 29,
				 30, 31, 32, 33, 34, 35, 36, 37, 67};
		public static System.Collections.Generic.List<int> PROBLEM_NUMBERS{
			get { return _PROBLEM_NUMBERS; }
		}

		//Returns the problem corresponding to the given problem number
		public static Problem GetProblem(int problemNumber){
			Problem problem = null;
			switch(problemNumber){
				case  1 : problem = new Problem1(); break;
				case  2 : problem = new Problem2(); break;
				case  3 : problem = new Problem3(); break;
				case  4 : problem = new Problem4(); break;
				case  5 : problem = new Problem5(); break;
				case  6 : problem = new Problem6(); break;
				case  7 : problem = new Problem7(); break;
				case  8 : problem = new Problem8(); break;
				case  9 : problem = new Problem9(); break;
				case 10 : problem = new Problem10(); break;
				case 11 : problem = new Problem11(); break;
				case 12 : problem = new Problem12(); break;
				case 13 : problem = new Problem13(); break;
				case 14 : problem = new Problem14(); break;
				case 15 : problem = new Problem15(); break;
				case 16 : problem = new Problem16(); break;
				case 17 : problem = new Problem17(); break;
				case 18 : problem = new Problem18(); break;
				case 19 : problem = new Problem19(); break;
				case 20 : problem = new Problem20(); break;
				case 21 : problem = new Problem21(); break;
				case 22 : problem = new Problem22(); break;
				case 23 : problem = new Problem23(); break;
				case 24 : problem = new Problem24(); break;
				case 25 : problem = new Problem25(); break;
				case 26 : problem = new Problem26(); break;
				case 27 : problem = new Problem27(); break;
				case 28 : problem = new Problem28(); break;
				case 29 : problem = new Problem29(); break;
				case 30 : problem = new Problem30(); break;
				case 31 : problem = new Problem31(); break;
				case 32 : problem = new Problem32(); break;
				case 33 : problem = new Problem33(); break;
				case 34 : problem = new Problem34(); break;
				case 35 : problem = new Problem35(); break;
				case 36 : problem = new Problem36(); break;
				case 37 : problem = new Problem37(); break;
				case 67 : problem = new Problem67(); break;
			}
			return problem;
		}
		//Print the description of a problem
		public static void PrintDescription(int problemNumber){
			//Get the problem
			Problem problem = GetProblem(problemNumber);
			//Print the problem's description
			System.Console.WriteLine(problem.Description);
		}
		//Solve a problem
		public static void SolveProblem(int problemNumber){
			//Get the problem
			Problem problem = GetProblem(problemNumber);
			//Print the problem description
			System.Console.WriteLine(problem.Description);
			//Solve the problem
			problem.Solve();
			//Print the results
			System.Console.WriteLine(problem.Result + "\nIt took " + problem.Time + " to solve this problem.\n\n");
		}
		//Get a valid problem number from a user
		public static int GetProblemNumber(){
			int problemNumber;
			System.Console.Write("Enter a problem number: ");
			string problemNumberString = System.Console.ReadLine();
			problemNumber = System.Convert.ToInt32(problemNumberString);
			while(_PROBLEM_NUMBERS.IndexOf(problemNumber) == -1){
				System.Console.Write("That is an invalid problem number!\nEnter a problem number: ");
				problemNumberString = System.Console.ReadLine();
				problemNumber = System.Convert.ToInt32(problemNumberString);
			}
			return problemNumber;
		}
		//List all valid problem numbers
		public static void ListProblems(){
			System.Console.Write(_PROBLEM_NUMBERS[1]);
			for(int problemNumber = 2; problemNumber < _PROBLEM_NUMBERS.Count; ++problemNumber){
				System.Console.Write(", " + _PROBLEM_NUMBERS[problemNumber]);
			}
			System.Console.WriteLine();
		}
	}
}
