﻿//ProjectEuler/ProjectEulerCS/src/Problems/Problem.cs
//Matthew Ellison
// Created: 08-14-20
//Modified: 07-05-21
//This is the base class for the rest of the problems
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


namespace ProjectEulerCS{
	public abstract class Problem{
		//Variables
		//Instance variables
		//The results of the problem
		public abstract string Result{get;}
		protected readonly string _description; //Holds the description of the problem
		public string Description{
			get{ return _description; }
		}
		protected bool solved;  //Shows whether the problem has already been solved
		protected mee.Stopwatch timer = new mee.Stopwatch();
		public mee.Stopwatch Timer{
			get{
				if(!solved){
					throw new Unsolved();
				}
				return timer;
			}
		}
		public string Time{
			get{
				if(!solved){
					throw new Unsolved();
				}
				return timer.GetStr();
			}
		}

		//Make sure the problem has been solved and throw an exception if not
		protected void SolvedCheck(string str){
			if(!solved){
				throw new Unsolved("You must solve the problem before you can see the " + str);
			}
		}


		//Constructor
		public Problem(string newDescription){
			_description = newDescription;
		}

		//Operations functions
		//Solve the problem
		public abstract void Solve();
		//Reset the problem so it can be run again
		public virtual void Reset(){
			timer.Reset();
			solved = false;
		}
	}
}
