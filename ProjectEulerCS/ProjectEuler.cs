﻿//ProjectEuler/ProjectEulerCS/src/ProblemSelection.cs
//Matthew Ellison
// Created: 08-21-20
//Modified: 08-21-20
//This is the driver function for the C# version of the ProjectEuler project
/*
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


namespace ProjectEulerCS{
	public class ProjectEuler{
		//An enum to hold the possible menu selections
		private enum SELECTIONS { SOLVE = 1, DESCRIPTION, LIST, BENCHMARK, EXIT, SIZE };

		//Drives the program
		public static void Main(){
			SELECTIONS selection;
			do{
				//Print the menu and prompt the user to select an action
				PrintMenu();
				selection = GetMenuSelection();

				switch(selection){
					case SELECTIONS.SOLVE: SolveMenu(); break;
					case SELECTIONS.DESCRIPTION: DescriptionMenu(); break;
					case SELECTIONS.LIST: ProblemSelection.ListProblems(); break;
					case SELECTIONS.BENCHMARK: Benchmark.BenchmarkMenu(); break;
					case SELECTIONS.EXIT: break;
					case SELECTIONS.SIZE:
					default: PrintErrorMessage(); break;
				}
			}while(!selection.Equals(SELECTIONS.EXIT));
		}
		//Print the menu
		private static void PrintMenu(){
			System.Console.WriteLine("1. Solve a problem");
			System.Console.WriteLine("2. Print a problem description");
			System.Console.WriteLine("3. List valid problem numbers");
			System.Console.WriteLine("4. Benchmark");
			System.Console.WriteLine("5. Exit");
			System.Console.WriteLine();
		}
		//Get a menu selection from the user
		private static SELECTIONS GetMenuSelection(){
			string selectionString = System.Console.ReadLine();
			int selection = System.Convert.ToInt32(selectionString);
			while(!IsValidMenu(selection)){
				System.Console.WriteLine("that is an invalid option!\nPress Enter to continue");
				PrintMenu();
				selectionString = System.Console.ReadLine();
				selection = System.Convert.ToInt32(selectionString);
			}
			return GetSelection(selection);
		}
		//Make sure the value passed in is a valid menu option
		private static bool IsValidMenu(int selection){
			if((selection >= (int)SELECTIONS.SOLVE) && (selection < (int)SELECTIONS.SIZE)){
				return true;
			}
			else{
				return false;
			}
		}
		//Turns an integer passed to it into a SELECTION enum
		private static SELECTIONS GetSelection(int selection){
			SELECTIONS sel = selection switch{
				1 => SELECTIONS.SOLVE,
				2 => SELECTIONS.DESCRIPTION,
				3 => SELECTIONS.LIST,
				4 => SELECTIONS.BENCHMARK,
				5 => SELECTIONS.EXIT,
				_ => SELECTIONS.SIZE,
			};
			return sel;
		}
		//Print an error message
		private static void PrintErrorMessage(){
			System.Console.WriteLine("That is an invalid selection!");
		}
		//Handle what happens when a user wants to solve a problem
		private static void SolveMenu(){
			int problemNumber = ProblemSelection.GetProblemNumber();
			//This selection solves all problems in order
			if(problemNumber == 0){
				//Solve to every valid problem number, skipping over 0
				for(int problemLocation = 1; problemLocation < ProblemSelection.PROBLEM_NUMBERS.Count; ++problemLocation){
					//Solve the problems
					System.Console.Write(ProblemSelection.PROBLEM_NUMBERS[problemLocation] + ". ");
					ProblemSelection.SolveProblem(ProblemSelection.PROBLEM_NUMBERS[problemLocation]);
				}
			}
			//This is if a single problem number was chosen
			else{
				//Solve the problem
				ProblemSelection.SolveProblem(problemNumber);
			}
		}
		//Handle what happens when a user wants to see the description of a problem
		private static void DescriptionMenu(){
			//Give some extra space to print the description
			System.Console.WriteLine("\n");

			//Get the problem number
			int problemNumber = ProblemSelection.GetProblemNumber();

			//If the problem number is 0 print out all the descriptions
			if(problemNumber == 0){
				//Print description for every valid problem number, skipping over 0
				for(int problemLocation = 1; problemLocation < ProblemSelection.PROBLEM_NUMBERS.Count; ++problemLocation){
					//Print the problem's descrfiption
					System.Console.Write(ProblemSelection.PROBLEM_NUMBERS[problemLocation] + ". ");
					ProblemSelection.PrintDescription(ProblemSelection.PROBLEM_NUMBERS[problemLocation]);
					System.Console.WriteLine();
				}
			}
			//Otherwise print out a single problem's description
			else{
				ProblemSelection.PrintDescription(problemNumber);
			}
		}
	}
}
