﻿//ProjectEuler/ProjectEulerCS/src/Problems/Problem1.cs
//Matthew Ellison
// Created: 08-14-20
//Modified: 07-05-21
//What is the sum of all the multiples of 3 or 5 that are less than 1000
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/CSClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


using System;


namespace ProjectEulerCS.Problems{
	public class Problem1 : Problem{
		//Variables
		//Static variables
		private const int TOP_NUM = 999;	//The largest number to tbe checked
		//Instance variables
		private int fullSum;	//The sum of all the numbers
		public int Sum{
			get{
				SolvedCheck("sum");
				return fullSum;
			}
		}

		//The results of the problem
		public override string Result{
			get{
				SolvedCheck("result");
				return $"The sum of all numbers < {TOP_NUM + 1} is {fullSum}";
			}
		}

		//Functions
		//Constructor
		public Problem1() : base($"What is the sum of all the multiples of 3 or 5 that are less than {TOP_NUM + 1}"){
			fullSum = 0;
		}
		//Operational functions
		//Solve the problem
		public override void Solve(){
			//If the problem has already been solved do nothing and end the function
			if (solved){
				return;
			}

			//Start the timer
			timer.Start();


			//Get the sum of the progressions of 3 and 5 and remove the sum of progressions of the overlap
			fullSum = SumOfProgression(3) + SumOfProgression(5) - SumOfProgression(3 * 5);


			//Stop the timer
			timer.Stop();

			//Thow a flag to show the problem is solved
			solved = true;
		}
		//Reset the problem so it can be run again
		public override void Reset(){
			base.Reset();
			fullSum = 0;
		}
		private int SumOfProgression(double multiple){
			double numTerms = Math.Floor(TOP_NUM / multiple);	//This gets the number of multiples of a particular number that is < MAX_NUMBER
			//The sum of progression formula is (n / 2)(a + l). n = number of terms, a = multiple, l = last term
			return (int)(numTerms / 2.0 * (multiple + (numTerms * multiple)));
		}
	}
}


/* Results:
The sum of all numbers < 1000 is 233168
It took an average of 1.351 microseconds to run this problem through 100 iterations
*/
