//ProjectEuler/ProjectEulerCS/src/Problems/Problem32.cs
//Matthew Ellison
// Created: 10-03-20
//Modified: 07-05-21
//Find the sum of all products whose multiplicand/multiplier/product identity can be written as a 1 through 9 pandigital.
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/CSClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


using System.Collections.Generic;


namespace ProjectEulerCS.Problems{
	public class Problem32 : Problem{
		//Structures
		//Holds the set of numbers that make a product
		private struct ProductSet{
			private readonly int multiplicand;
			private readonly int multiplier;
			public int Multiplicand{
				get{
					return multiplicand;
				}
			}
			public int Multiplier{
				get{
					return multiplier;
				}
			}
			public int Product{
				get{
					return (multiplicand * multiplier);
				}
			}
			public ProductSet(int multiplicand, int multiplier){
				this.multiplicand = multiplicand;
				this.multiplier = multiplier;
			}
			public override bool Equals(object obj){
				if(obj == null || GetType() != obj.GetType()){
					return false;
				}
				ProductSet secondSet = (ProductSet)obj;

				//Return true if the products are the same
				return (Product == secondSet.Product);
			}
			public override int GetHashCode(){
				return multiplicand ^ multiplier;
			}
			public override string ToString(){
				return $"{multiplicand}{multiplier}{Product}";
			}
		}

		//Variables
		//Static variables
		private const int TOP_MULTIPLICAND = 99;	//The largest multiplicand to check
		private const int TOP_MULTIPLIER = 4999;	//The largest multiplier to check
		//Instance variables
		private readonly List<ProductSet> listOfProducts;	//The list of unique products that are 1-9 pandigital
		private long sumOfPandigitals;	//THe sum of the products of the pandigital numbers
		//Gets
		public long SumOfPandigitals{
			get{
				SolvedCheck("sum of the pandigitals");
				return sumOfPandigitals;
			}
		}
		public override string Result{
			get{
				SolvedCheck("result");
				return $"There are {listOfProducts.Count} unique 1-9 pandigitals\nThe sum of the products of these pandigitals is {sumOfPandigitals}";
			}
		}

		//Functions
		//Constructor
		public Problem32() : base("Find the sum of all products whose multiplicand/multiplier/product identity can be written as a 1 through 9 pandigital."){
			listOfProducts = new List<ProductSet>();
			sumOfPandigitals = 0;
		}
		//Operational functions
		//Solve the problem
		public override void Solve(){
			//If the problem has already been solved do nothing and end the function
			if(solved){
				return;
			}

			//Start the timer
			timer.Start();


			//Create the multiplicand and start working your way up
			for(int multiplicand = 1;multiplicand <= TOP_MULTIPLICAND;++multiplicand){
				//Run through all possible multipliers
				for(int multiplier = multiplicand;multiplier <= TOP_MULTIPLIER;++multiplier){
					ProductSet currentProductSet = new ProductSet(multiplicand, multiplier);
					//If the product is too long move on the the next possible number
					if(currentProductSet.ToString().Length > 9){
						break;
					}
					//If the current number is pandigital that doesn't already exist in the list add it to the list
					if(IsPandigital(currentProductSet)){
						if(!listOfProducts.Contains(currentProductSet)){
							listOfProducts.Add(currentProductSet);
						}
					}
				}
			}

			//Get the sum of the products of the pandigitals
			foreach(ProductSet prod in listOfProducts){
				sumOfPandigitals += prod.Product;
			}


			//Stop the timer
			timer.Stop();

			//Throw a flag to show the problem is solved
			solved = true;
		}
		//Returns true if the passed productset is 1-9  pandigital
		private bool IsPandigital(ProductSet currentSet){
			//Get the numbers out of the object and put them into a string
			string numberString = currentSet.ToString();
			//Make sure the string is the correct length
			if(numberString.Length != 9){
				return false;
			}
			//Make sure every number from 1-9 is contained exactly once
			for(int panNumber = 1;panNumber <= 9;++panNumber){
				//Make sure there is exactly one of this number contained in the string
				if(mee.StringAlgorithms.FindNumOccurrence(numberString, panNumber.ToString()[0]) != 1){
					return false;
				}
			}
			//If all numbers were found in the string return true
			return true;
		}
		//Reset the problem so it can be run again
		public override void Reset(){
			base.Reset();
			listOfProducts.Clear();
			sumOfPandigitals = 0;
		}
	}
}


/* Results:
There are 7 unique 1-9 pandigitals
The sum of the products of these pandigitals is 45228
It took an average of 16.960 milliseconds to run this problem through 100 iterations
*/
