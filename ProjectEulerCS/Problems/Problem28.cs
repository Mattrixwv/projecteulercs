//ProjectEuler/ProjectEulerCS/src/Problems/Problem28.cs
//Matthew Ellison
// Created: 09-21-20
//Modified: 07-05-21
//What is the sum of the numbers on the diagonals in a 1001 by 1001 spiral formed by starting with the number 1 and moving to the right in a clockwise direction a 5 by 5 spiral
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/CSClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


using System.Collections.Generic;


namespace ProjectEulerCS.Problems{
	public class Problem28 : Problem{
		//Variables
		//Static variables
		private static List<List<int>> grid;	//Holds the grid that we will be filling and searching
		//Instance variables
		private int sumOfDiagonals;	//Holds the sum of the diagonals of the grid
		public List<List<int>> Grid{
			get{
				SolvedCheck("grid");
				return grid;
			}
		}
		public int Sum{
			get{
				SolvedCheck("sum");
				return sumOfDiagonals;
			}
		}
		public override string Result{
			get{
				SolvedCheck("result");
				return $"The sum of the diagonals in the given grid is {sumOfDiagonals}";
			}
		}

		//Functions
		//Constructor
		public Problem28() : base("What is the sum of the numbers on the diagonals in a 1001 by 1001 spiral formed by starting with the number 1 and moving to the right in a clockwise direction?"){
			sumOfDiagonals = 0;
		}
		//Operational functions
		//Sets up the grid
		private void SetupGrid(){
			grid = new List<List<int>>();
			//Fill the grid with 0's
			for(int cnt = 0;cnt < 1001;++cnt){
				//Add a blank List
				grid.Add(new List<int>());
				for(int cnt2 = 0;cnt2 < 1001;++cnt2){
					grid[cnt].Add(0);
				}
			}

			bool finalLocation = false;	//A flag to indicate if the final location to be filled has been reached
			//Set the number that is going to be put at each location
			int currentNum = 1;
			//Start with the middle location and set it correctly and advance the tracker to the next number
			int xLocation = 500;
			int yLocation = 500;
			grid[yLocation][xLocation] = currentNum++;
			//Move right the first time
			++xLocation;
			//Move in a circular pattern until you reach the final location
			while(!finalLocation){
				//Move down until you reach a blank location on the left
				while(grid[yLocation][xLocation - 1] != 0){
					grid[yLocation][xLocation] = currentNum++;
					++yLocation;
				}

				//Move left until you reach a blank location above
				while(grid[yLocation - 1][xLocation] != 0){
					grid[yLocation][xLocation] = currentNum++;
					--xLocation;
				}

				//Move up until you reach a blank location to the right
				while(grid[yLocation][xLocation + 1] != 0){
					grid[yLocation][xLocation] = currentNum++;
					--yLocation;
				}

				//Move right until you reach a blank location below
				while(grid[yLocation + 1][xLocation] != 0){
					grid[yLocation][xLocation] = currentNum++;
					++xLocation;
					//Check if you are at the final location and break the loop if you are
					if(xLocation == grid.Count){
						finalLocation = true;
						break;
					}
				}
			}
		}
		//Finds the sum of the diagonals in the grid
		private void FindSum(){
			//Start at the top corners and work your way down moving toward the opposite side
			int leftSide = 0;
			int rightSide = grid.Count - 1;
			int row = 0;
			while(row < grid.Count){
				//This ensures the middle location is only counted once
				if(leftSide == rightSide){
					sumOfDiagonals += grid[row][leftSide];
				}
				else{
					sumOfDiagonals += grid[row][leftSide];
					sumOfDiagonals += grid[row][rightSide];
				}
				++row;
				++leftSide;
				--rightSide;
			}
		}
		//Solve the problem
		public override void Solve(){
			//If the problem has already been solved do nothing and end the function
			if(solved){
				return;
			}

			//Start the timer
			timer.Start();


			//Setup the grid
			SetupGrid();
			//Find the sum of the diagonals in the grid
			FindSum();


			//Stop the timer
			timer.Stop();

			//Throw a flag to show the problem is solved
			solved = true;
		}
		//Reset the problem so it can be run again
		public override void Reset(){
			base.Reset();
			sumOfDiagonals = 0;
		}
	}
}


/* Results:
The sum of the diagonals in the given grid is 669171001
It took an average of 7.735 milliseconds to run this problem through 100 iterations
*/
