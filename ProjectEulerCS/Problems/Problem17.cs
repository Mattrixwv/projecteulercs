//ProjectEuler/ProjectEulerCS/src/Problems/Problem17.cs
//Matthew Ellison
// Created: 08-25-20
//Modified: 07-05-21
//If all the numbers from 1 to 1000 (one thousand) inclusive were written out in words, how many letters would be used?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/CSClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


using System;


namespace ProjectEulerCS.Problems{
	public class Problem17 : Problem{
		//Variables
		//Static variables
		private const int START_NUM = 1;	//This is the smallest number to get the words of
		private const int STOP_NUM = 1000;	//This is the largest number to get the words of
		//Instance variables
		private long letterCounter;	//This is the cumulative number of letters in the words of the numbers
		public long LetterCount{
			get{
				SolvedCheck("letter count");
				return letterCounter;
			}
		}

		//The results of the problem
		public override string Result{
			get{
				SolvedCheck("result");
				return $"The sum of all the letters in all the numbers {START_NUM}-{STOP_NUM} is {letterCounter}";
			}
		}

		//Functions
		//Constructor
		public Problem17() : base($"If all the numbers from {START_NUM} to {STOP_NUM} inclusive were written out in words, how many letters would be used?"){
			letterCounter = 0;
		}
		//Operational functions
		//Solve the problem
		public override void Solve(){
			//If the problem has already been solved do nothing and end the function
			if(solved){
				return;
			}

			//Start the timer
			timer.Start();


			//Start with 1 and increment
			for(int num = START_NUM;num <= STOP_NUM;++num){
				//Pass the number to a function that will create a string for the number
				string currentNumString = GetStringFromNum(num);
				//Pass the string to the function that will count the number of letters in it, ignoring whitespace and punctuation and add that number to the running tally
				letterCounter += GetNumberChars(currentNumString);
			}


			//Stop the timer
			timer.Stop();

			//Throw a flag to show the problem is solved
			solved = true;
		}
		//Reset the problem so it can be run again
		public override void Reset(){
			base.Reset();
			letterCounter = 0;
		}
		//This function makes a word out of the number passed into it
		private string GetStringFromNum(int number){
			string numberString = "";
			//Starting with the largest digit create a string based on the number passed in
			//Check for negative
			if(number < 0){
				numberString += "negative ";
			}

			//Check if the number is zero
			if(number == 0){
				numberString += "zero";
			}

			//Start with the thousands place
			if((number / 1000D) >= 1D){
				numberString += GetStringFromNum((int)System.Math.Floor(number / 1000D));
				numberString += " thousand";
				number -= (((int)Math.Floor(number / 1000D)) * 1000);
			}

			//Check the hundreds place
			if((number / 100D) >= 1D){
				numberString += GetStringFromNum((int)Math.Floor(number / 100D));
				numberString += " hundred";
				number -= (((int)Math.Floor(number / 100D)) * 100);
			}

			//Insert an and if there is need
			if((numberString.Length > 0) && (number > 0)){
				numberString += " and ";
			}

			//Check for tens place
			if((number / 10D) >= 2D){
				//For the tens you need to do something special
				int tensPlace = (int)Math.Floor(number / 10D);
				switch(tensPlace){
					case 9: numberString += "ninety"; break;
					case 8: numberString += "eighty"; break;
					case 7: numberString += "seventy"; break;
					case 6: numberString += "sixty"; break;
					case 5: numberString += "fifty"; break;
					case 4: numberString += "forty"; break;
					case 3: numberString += "thirty"; break;
					case 2: numberString += "twenty"; break;
				}
				number -= (tensPlace * 10);
				//If there is something left in the number you will need a dash to separate the tens and ones place
				if(number > 0){
					numberString += "-";
				}
			}
			//Check for teens
			else if((number / 10D) >= 1D){
				int onesPlace = (number % 10);
				switch(onesPlace){
					case 9: numberString += "nineteen"; break;
					case 8: numberString += "eighteen"; break;
					case 7: numberString += "seventeen"; break;
					case 6: numberString += "sixteen"; break;
					case 5: numberString += "fifteen"; break;
					case 4: numberString += "fourteen"; break;
					case 3: numberString += "thirteen"; break;
					case 2: numberString += "twelve"; break;
					case 1: numberString += "eleven"; break;
					case 0: numberString += "ten"; break;
				}
				//If this was hit the number was completed
				number = 0;
			}

			//Check for the ones place
			if(number >= 1){
				switch(number){
					case 9: numberString += "nine"; break;
					case 8: numberString += "eight"; break;
					case 7: numberString += "seven"; break;
					case 6: numberString += "six"; break;
					case 5: numberString += "five"; break;
					case 4: numberString += "four"; break;
					case 3: numberString += "three"; break;
					case 2: numberString += "two"; break;
					case 1: numberString += "one"; break;
				}
				//If this was hit the number was completed
				number = 0;
			}

			if(number != 0){
				throw new mee.Exceptions.InvalidResult("The number was not reduced to 0!");
			}

			//Return the string
			return numberString;
		}
		//This counts the number of letters in the string that is passed in (ignoring numbers and punctuation)
		private int GetNumberChars(String number){
			int sumOfLetters = 0;
			//Start at location 0 and count the number of letters, ignoring punctuation and whitespace
			for(int location = 0;location < number.Length;++location){
				if(Char.IsLetter(number[location])){
					sumOfLetters += 1;
				}
			}

			//Return the number of letters
			return sumOfLetters;
		}
	}
}


/* Result:
The sum of all the letters in all the numbers 1-1000 is 21124
It took an average of 208.222 microseconds to run this problem through 100 iterations
*/
