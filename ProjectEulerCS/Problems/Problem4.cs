//ProjectEuler/ProjectEulerCS/src/Problems/Problem4.cs
//Matthew Ellison
// Created: 08-23-20
//Modified: 07-05-21
//Find the largest palindrome made from the product of two 3-digit numbers
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/CSClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


using System.Collections.Generic;
using System.Linq;


namespace ProjectEulerCS.Problems{
	public class Problem4 : Problem{
		//Variables
		//Static variables
		private const int START_NUM = 100;	//The first number to be multiplied
		private const int END_NUM = 999;	//The last number to be multiplied
		//Instace variable
		private readonly List<int> palindromes;	//Holds all numbers that turn out to be palindromes
		public List<int> Palindromes{
			get{
				SolvedCheck("palindromes");
				return palindromes;
			}
		}
		public int LargestPalindrom{
			get{
				SolvedCheck("largest palindrome");
				return palindromes[^1];
			}
		}

		//The results of the problem
		public override string Result{
			get{
				SolvedCheck("result");
				return $"The largest palindrome is {palindromes[^1]}";
			}
		}

		//Constructor
		public Problem4() : base("Find the largest palindrome made from the product of two 3-digit numbers"){
			palindromes = new List<int>();
		}
		//Operational funcitons
		//SOlve the problem
		public override void Solve(){
			//If the problem has already been solved do nothing and end the function
			if(solved){
				return;
			}

			//Start the timer
			timer.Start();


			//Start at the first 3-digit number and check every one up to the last 3-digit number
			for(int firstNum = START_NUM;firstNum <= END_NUM;++firstNum){
				//You can start at the location of the first number because everything before that has already been tested. (100 * 101 == 101 * 100)
				for(int secondNum = firstNum;secondNum < END_NUM;++secondNum){
					//Get the product
					int product = firstNum * secondNum;
					//Change the number into a string
					string productString = product.ToString();
					//Reverse the string
					string reverseString = new string(productString.ToCharArray().Reverse().ToArray());

					//If the number and it's reverse are the same it is a palindrom so add it to the list
					if(productString == reverseString){
						palindromes.Add(product);
					}
					//If it's not a palindrome ignore it and move to the next number
				}
			}

			//Sort the palindromes so that the last one is the largest
			palindromes.Sort();


			//Stop the timer
			timer.Stop();

			//Throw a flag to show the problem is solved
			solved = true;
		}
		//Reset the problem so it can be run again
		public override void Reset(){
			base.Reset();
			palindromes.Clear();
		}
	}
}


/* Results:
The largest palindrome is 906609
It took an average of 51.682 milliseconds to run this problem through 100 iterations
*/
