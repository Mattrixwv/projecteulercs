//ProjectEuler/ProjectEulerCS/src/Problems/Problem36.cs
//Matthew Ellison
// Created: 06-29-21
//Modified: 07-05-21
//Find the sum of all numbers, less than one million, which are palindromic in base 10 and base 2.
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/CSClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


using System.Collections.Generic;


namespace ProjectEulerCS.Problems{
	public class Problem36 : Problem{
		//Variables
		//Static variables
		private static readonly int MAX_NUM = 999999;	//The largest number that will be checked
		//Instance variables
		private readonly List<int> palindromes;	//All numbers that are palindromes in base 10 and 2
		private int sum;	//The sum of all elements in the list of palindromes
		//Gets
		//The results of the problem
		public override string Result{
			get{
				SolvedCheck("result");
				return $"The sum of all base 10 and base 2 palindromic numbers < {MAX_NUM} is {sum}";
			}
		}
		public List<int> Palindromes{
			get{
				SolvedCheck("list of palindromes");
				return palindromes;
			}
		}
		public int SumOfPalindromes{
			get{
				SolvedCheck("sum of all palindromes");
				return sum;
			}
		}

		//Functions
		//Constructor
		public Problem36() : base("Find the sum of all numbers, less than one million, which are palindromic in base 10 and base 2."){
			palindromes = new List<int>();
			sum = 0;
		}
		//Operational functions
		//Solve the problem
		public override void Solve(){
			//If the problem has already been solved do nothing and end the function
			if(solved){
				return;
			}

			//Start the timer
			timer.Start();


			//Start with 1, check if it is a palindrome in base 10 and 2, and continue to MAX_NUM
			for(int num = 1;num < MAX_NUM;++num){
				//Check if num is a palindrome
				if(mee.StringAlgorithms.IsPalindrome(num.ToString())){
					//Convert num to base 2 and see if that is a palindrome
					string binNum = mee.NumberAlgorithms.ToBin(num);
					if(mee.StringAlgorithms.IsPalindrome(binNum)){
						//Add num to the list of palindromes
						palindromes.Add(num);
					}
				}
			}
			//Get the sum of all palindromes in the list
			sum = mee.ArrayAlgorithms.GetSum(palindromes);


			//Stop the timer
			timer.Stop();

			//Throw a flag to show the problem is solved
			solved = true;
		}
		//Reset the problem so it can be run again
		public override void Reset(){
			base.Reset();
			palindromes.Clear();
			sum = 0;
		}
	}
}


/* Results:
The sum of all base 10 and base 2 palindromic numbers < 999999 is 872187
It took an average of 140.447 milliseconds to run this problem through 100 iterations
*/
