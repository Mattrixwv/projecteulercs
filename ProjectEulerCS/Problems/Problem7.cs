//ProjectEuler/ProjectEulerCS/src/Problems/Problem7.cs
//Matthew Ellison
// Created: 08-23-20
//Modified: 07-05-21
//What is the 10001th prime number?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/CSClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


using System.Collections.Generic;


namespace ProjectEulerCS.Problems{
	public class Problem7 : Problem{
		//Variables
		//Static variables
		private const long NUMBER_OF_PRIMES = 10001;	//The number of primes we are trying to get
		//Instance variables
		private List<long> primes;
		public long Prime{
			get{
				SolvedCheck("prime");
				return primes[^1];
			}
		}

		//The results of the problem
		public override string Result{
			get{
				SolvedCheck("result");
				return $"The {NUMBER_OF_PRIMES}th prime number is {primes[^1]}";
			}
		}

		//Functions
		//Constructor
		public Problem7() : base($"What is the {NUMBER_OF_PRIMES}th prime number?"){
			primes = new List<long>();
		}
		//Operatinal functions
		//Solve the problem
		public override void Solve(){
			//If the problem has already been solved do nothing and end the function
			if(solved){
				return;
			}

			//Start the timer
			timer.Start();


			//Setup the variables
			primes = mee.NumberAlgorithms.GetNumPrimes(NUMBER_OF_PRIMES);	//Holds the prime numbers


			//Stop the timer
			timer.Stop();

			//Throw a flag to show the problem is solved
			solved = true;
		}
		//Reset the problem so it can be run again
		public override void Reset(){
			base.Reset();
			primes.Clear();
		}
	}
}


/* Results:
The 10001th prime number is 104743
It took an average of 3.559 milliseconds to run this problem through 100 iterations
*/
