//ProjectEuler/ProjectEulerCS/src/Problems/Problem26.cs
//Matthew Ellison
// Created: 09-11-20
//Modified: 07-05-21
//Find the value of d < 1000 for which 1/d contains the longest recurring cycle in its decimal fraction part.
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/CSClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


using System.Collections.Generic;


namespace ProjectEulerCS.Problems{
	public class Problem26 : Problem{
		//Variables
		//Static variables
		private const int TOP_NUM = 999;	//The number of digits needed in the number
		//Instance variables
		private int longestCycle;	//The length of the longest cycle
		private int longestNumber;	//The starting denominator of the longest cycle
		public int LongestCycle{
			get{
				SolvedCheck("length of the longest cycle");
				return longestCycle;
			}
		}
		public int LongestNumber{
			get{
				SolvedCheck("denominator that starts the longest cycle");
				return longestNumber;
			}
		}
		public override string Result{
			get{
				SolvedCheck("result");
				return $"The longest cycle is {longestCycle} digits long\nIt started with the number {longestNumber}";
			}
		}

		//Functions
		//Constructor
		public Problem26() : base($"Find the value of d <= {TOP_NUM} for which 1/d contains the longest recurring cycle in its decimal fraction part."){
			longestCycle = 0;
			longestNumber = 0;
		}
		//Operational functions
		//Solve the problem
		public override void Solve(){
			//If the problem has already been solved do nothing and end the function
			if(solved){
				return;
			}

			//Start the timer
			timer.Start();


			//Start with 1/2 and find out how long the longest cycle is by checking the remainders
			//Loop through every number from 2-999 and use it for the denominator
			for(int denominator = 2;denominator <= TOP_NUM;++denominator){
				List<int> denomList = new List<int>();
				bool endFound = false;	//A flag for when we have found an end to the number (either a cycle or a 0 for remainder)
				bool cycleFound = false;	//A flag to indicate a cycle was detected
				int numerator = 1;	//The numerator that will be divided. Always starts at 1
				while(!endFound){
					//Get the remainder after the division
					int remainder = numerator % denominator;
					//Check if the remainder is 0 and set the flag
					if(remainder == 0){
						endFound = true;
					}
					//Check if the remainder is in the list and set the appropriate flags
					else if(denomList.Contains(remainder)){
						endFound = true;
						cycleFound = true;
					}
					//Else add it to the list
					else{
						denomList.Add(remainder);
					}
					//Multiply the remainder by 10 to continue finding the next remainder
					numerator = remainder * 10;
				}
				//If a cycle was found check the size of the list against the largest cycle
				if(cycleFound){
					//If it is larger than the largest, set it as the new largest
					if(denomList.Count > longestCycle){
						longestCycle = denomList.Count;
						longestNumber = denominator;
					}
				}
			}


			//Stop the timer
			timer.Stop();

			//Throw a flag to show the problem is solved
			solved = true;
		}
		//Reset the problem so it can be run again
		public override void Reset(){
			base.Reset();
			longestCycle = 0;
			longestNumber = 1;
		}
	}
}


/* Results:
The longest cycle is 982 digits long
It started with the number 983
It took an average of 4.657 milliseconds to run this problem through 100 iterations
*/
