//ProjectEuler/ProjectEulerCS/src/Problems/Problem31.cs
//Matthew Ellison
// Created: 10-03-20
//Modified: 07-05-21
//How many different ways can £2 be made using any number of coins?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/CSClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


namespace ProjectEulerCS.Problems{
	public class Problem31 : Problem{
		//Variables
		//Static variables
		private const int desiredValue = 200;	//The value of coins we want
		//Instance variables
		private int permutations;	//The number of permutations that are found
		public int Permutations{
			get{
				SolvedCheck("number of correct permutations of the coins");
				return permutations;
			}
		}
		public override string Result{
			get{
				SolvedCheck("result");
				return $"There are {permutations} ways to make 2 pounds with the given denominations of coins";
			}
		}

		//Functions
		//Constructor
		public Problem31() : base("How many different ways can 2 pounds be made using any number of coins?"){
			permutations = 0;
		}
		//Operational functions
		//Solve the problem
		public override void Solve(){
			//If the problem has already been solved do nothing and end the function
			if(solved){
				return;
			}

			//Start the timer
			timer.Start();


			//Start with 200p and remove the necessary coins with each loop
			for(int pound2 = desiredValue; pound2 >= 0;pound2 -= 200){
				for(int pound1 = pound2;pound1 >= 0;pound1 -= 100){
					for(int pence50 = pound1;pence50 >= 0;pence50 -= 50){
						for(int pence20 = pence50;pence20 >= 0;pence20 -= 20){
							for(int pence10 = pence20;pence10 >= 0;pence10 -= 10){
								for(int pence5 = pence10;pence5 >= 0;pence5 -= 5){
									for (int pence2 = pence5; pence2 >= 0; pence2 -= 2){
										++permutations;
									}
								}
							}
						}
					}
				}
			}


			//Stop the timer
			timer.Stop();

			//Throw a flag to show the problem is solved
			solved = true;
		}
		//Reset the problem so it can be run again
		public override void Reset(){
			base.Reset();
			permutations = 0;
		}
	}
}


/* Results:
There are 73682 ways to make 2 pounds with the given denominations of coins
It took an average of 117.971 microseconds to run this problem through 100 iterations
*/
