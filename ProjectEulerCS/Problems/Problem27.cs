//ProjectEuler/ProjectEulerCS/src/Problems/Problem27.cs
//Matthew Ellison
// Created: 09-11-20
//Modified: 07-05-21
//Find the product of the coefficients, |a| < 1000 and |b| <= 1000, for the quadratic expression that produces the maximum number of primes for consecutive values of n, starting with n=0.
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/CSClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


using System.Collections.Generic;


namespace ProjectEulerCS.Problems{
	public class Problem27 : Problem{
		//Variables
		//Static variables
		private const int LARGEST_POSSIBLE_A = 999;
		private const int LARGEST_POSSIBLE_B = 1000;
		//Instance variables
		private int topA;	//The A for the most n's generated
		private int topB;	//THe B for the most n's generated
		private int topN;	//The most n's generated
		private List<int> primes;	//A list of all primes that could possibly be generated with this formula
		public int TOP_A{
			get{
				SolvedCheck("largest A");
				return topA;
			}
		}
		public int TOP_B{
			get{
				SolvedCheck("largest B");
				return topB;
			}
		}
		public int TOP_N{
			get{
				SolvedCheck("largest N");
				return topN;
			}
		}
		public int Product{
			get{
				SolvedCheck("product of A and B");
				return topA * topB;
			}
		}
		public override string Result{
			get{
				SolvedCheck("result");
				return $"The greatest number of primes found is {topN}\nIt was found with A = {topA}, B = {topB}\nThe product of A and B is {topA * topB}";
			}
		}

		//Functions
		//Constructor
		public Problem27() : base($"Find the product of the coefficients, |a| <= {LARGEST_POSSIBLE_A} and |b| <= {LARGEST_POSSIBLE_B}, for the quadratic expression that produces the maximum number of primes for consecutive values of n, starting with n=0"){
			topA = 0;
			topB = 0;
			topN = 0;
			primes = new List<int>();
		}
		//Operational functions
		//Solve the problem
		public override void Solve(){
			//If the problem has already been solved do nothing and end the function
			if(solved){
				return;
			}

			//Start the timer
			timer.Start();


			//Get the primes
			primes = mee.NumberAlgorithms.GetPrimes(12000);

			//Start with the lowest possible A and check all possibilities after that
			for(int a = -LARGEST_POSSIBLE_A;a <= LARGEST_POSSIBLE_A;++a){
				//Start with the lowest possible B and check all possibilities after that
				for(int b = -LARGEST_POSSIBLE_B;b <= LARGEST_POSSIBLE_B;++b){
					//Start with n=0 and check the formula to see how many primes you can get with concecutive n's
					int n = 0;
					int quadratic = (n * n) + (a * n) + b;
					while(primes.Contains(quadratic)){
						++n;
						quadratic = (n * n) + (a * n) + b;
					}
					--n;	//Negate an n because the last formula failed'

					//Set all the largest numbers if this created more primes than any other
					if(n > topN){
						topN = n;
						topB = b;
						topA = a;
					}
				}
			}


			//Top the timer
			timer.Stop();

			//Throw a flag to show the problem is solved
			solved = true;
		}
		//Reset the problem so it can be run again
		public override void Reset(){
			base.Reset();
			topA = 0;
			topB = 0;
			topN = 0;
			primes.Clear();
		}
	}
}


/* Result:
The greatest number of primes found is 70
It was found with A = -61, B = 971
The product of A and B is -59231
It took an average of 1.394 seconds to run this problem through 100 iterations
*/
