//ProjectEuler/ProjectEulerCS/src/Problems/Problem12.cs
//Matthew Ellison
// Created: 08-24-20
//Modified: 07-05-21
//What is the value of the first triangle number to have over five hundred divisors?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/CSClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


using System.Collections.Generic;


namespace ProjectEulerCS.Problems{
	public class Problem12 : Problem{
		//Variables
		//Static variables
		private const int GOAL_DIVISORS = 500;

		//Instance variables
		private long sum;	//The sum of the numbers up to counter
		private long counter;	//The next number to be added to sum
		private List<long> divisors;	//Holds the divisors of the triangular number sum
		public long TriangularNumber{
			get{
				SolvedCheck("triangular number");
				return sum;
			}
		}
		public long LastNumberAdded{
			get{
				SolvedCheck("last number added to get the triangular number");
				return counter - 1;
			}
		}
		public List<long> DivisorsOfTriangularNumber{
			get{
				SolvedCheck("divisors of the triangular number");
				return divisors;
			}
		}
		public int NumberOfDivisors{
			get{
				SolvedCheck("number of divisors of the triangular number");
				return divisors.Count;
			}
		}

		//The result of the problem
		public override string Result{
			get{
				SolvedCheck("result");
				return $"The triangular number {sum} is the sum of all numbers >= {counter - 1} and has {divisors.Count} divisors";
			}
		}

		//Functions
		//Constructor
		public Problem12() : base("What is the value of the first triangle number to have over five hundred divisors?"){
			sum = 1;
			counter = 2;
			divisors = new List<long>();
		}
		//Operational functions
		//Solve the problem
		public override void Solve(){
			//If the problem has already been solved do nothing and end the function
			if(solved){
				return;
			}

			//Setup the other variables
			bool foundNumber = false;	//To flag whether the number has been found

			//Start the timer
			timer.Start();


			//Loop until you fin the appropriate number
			while((!foundNumber) && (sum > 0)){
				divisors = mee.NumberAlgorithms.GetDivisors(sum);
				//If the number of divisors is correct set the flag
				if(divisors.Count > GOAL_DIVISORS){
					foundNumber = true;
				}
				//Otherwise add to the sum and increase the next number
				else{
					sum += counter;
					++counter;
				}
			}


			//Stop the timer
			timer.Stop();

			//Throw a flag to show the problem is sovled
			solved = true;
		}
		//Reset the problem so it can be run again
		public override void Reset(){
			base.Reset();
			sum = 1;
			counter = 2;
			divisors.Clear();
		}
	}
}


/* Results:
The triangular number 76576500 is the sum of all numbers >= 12375 and has 576 divisors
It took an average of 270.496 milliseconds to run this problem through 100 iterations
*/
