//ProjectEuler/ProjectEulerCS/src/Problems/Problem14.cs
//Matthew Ellison
// Created: 08-24-20
//Modified: 07-05-21
/*
The following iterative sequence is defined for the set of positive integers:
n → n/2 (n is even)
n → 3n + 1 (n is odd)
Which starting number, under one million, produces the longest chain?
*/
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/CSClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


namespace ProjectEulerCS.Problems{
	public class Problem14 : Problem{
		//Variables
		//Static variables
		private const long MAX_NUM = 1000000 - 1;	//This is the top number that you will be checking against the series
		//Instance variables
		private long maxLength;	//This is the length of the longest chain
		public long Length{
			get{
				SolvedCheck("length of the longest chain");
				return maxLength;
			}
		}
		private long maxNum;	//This is the starting number of the longest chain
		public long StartingNumber{
			get{
				SolvedCheck("starting number of the longest chain");
				return maxNum;
			}
		}

		public override string Result{
			get{
				SolvedCheck("result");
				return $"The number {maxNum} produced a chain of {maxLength} steps";
			}
		}

		//Functions
		//Constructor
		public Problem14() : base($"Which starting number, under {MAX_NUM + 1}, produces the longest chain using the itterative sequence?"){
			maxLength = 0;
			maxNum = 0;
		}
		//Operational functions
		//Solve the problem
		public override void Solve(){
			//If the problem has already been solved do nothing and end the function
			if(solved){
				return;
			}

			//Start the timer
			timer.Start();


			//Loop through all numbers <= MAX_NUM and check them against the series
			for(long currentNum = 1;currentNum <= MAX_NUM;++currentNum){
				long currentLength = CheckSeries(currentNum);
				//If the current number has a longer series than the max then the current becomes the max
				if(currentLength > maxLength){
					maxLength = currentLength;
					maxNum = currentNum;
				}
			}


			//Stop the timer
			timer.Stop();

			//Throw a flag to show the problem is solved
			solved = true;
		}
		//This function follows the rules of the sequence and returns its length
		private long CheckSeries(long num){
			long length = 1;	//Start at 1 because you need to count the starting number

			//Follow the series, adding 1 for each time you take
			while(num > 1){
				if((num % 2) == 0){
					num /= 2;
				}
				else{
					num = (3 * num) + 1;
				}
				++length;
			}

			//Return the length of the series
			return length;
		}
		//Reset the problem so it can be run again
		public override void Reset(){
			base.Reset();
			maxLength = 0;
			maxNum = 0;
		}
	}
}


/* Results:
The number 837799 produced a chain of 525 steps
It took an average of 249.883 milliseconds to run this problem through 100 iterations
*/
