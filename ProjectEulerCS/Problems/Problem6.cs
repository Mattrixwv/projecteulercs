//ProjectEuler/ProjectEulerCS/src/Problems/Problem6.cs
//Matthew Ellison
// Created: 08-23-20
//Modified: 07-05-21
//Find the difference between the sum of the squares of the first one hundred natural numbers and the square of the sum.
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/CSClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


using System;


namespace ProjectEulerCS.Problems{
	public class Problem6 : Problem{
		//Variables
		//Static variables
		private const int START_NUM = 1;	//The first number that needs to be counted
		private const int END_NUM = 100;	//The last number that needs to be counted
		//Instance variables
		private long sumOfSquares;	//Holds the sum of the squares of all the numbers
		public long SumOfSquares{
			get{
				SolvedCheck("sum of the squares");
				return sumOfSquares;
			}
		}
		private long squareOfSum;	//Holds the square of the sum of all the numbers
		public long SquareOfSum{
			get{
				SolvedCheck("square of the sums");
				return squareOfSum;
			}
		}
		public long Difference{
			get{
				SolvedCheck("difference between the two numbers");
				return Math.Abs(sumOfSquares - squareOfSum);
			}
		}

		//The results of the problem
		public override string Result{
			get{
				SolvedCheck("result");
				return $"The difference between the sum of the squares and the square of the sum of all numbers from 1-100 is {Math.Abs(sumOfSquares - squareOfSum)}";
			}
		}
		//Functions
		//Constructor
		public Problem6() : base($"Find the difference between the sum of the squares and the square of the sum of the numbers {START_NUM}-{END_NUM}."){
			sumOfSquares = 0;
			squareOfSum = 0;
		}
		//Operational functions
		//Solve the problem
		public override void Solve(){
			//If the problem has already been solved do nothing and end the function
			if(solved){
				return;
			}

			//Start the timer
			timer.Start();


			//Run through all numbers and add them to the appropriate sums
			for(int currentNum = START_NUM;currentNum <= END_NUM;++currentNum){
				sumOfSquares += (currentNum * currentNum);	//Add the square to the correct variable
				squareOfSum += currentNum;	//Add the number to the correct variable to squaring later
			}
			//Squaring the sum that needs it
			squareOfSum *= squareOfSum;


			//Stop the timer
			timer.Stop();

			//Throw a flag to show the problem is solved
			solved = true;
		}
		//Reset the problem so it can be run again
		public override void Reset(){
			base.Reset();
			squareOfSum = 0;
			sumOfSquares = 0;
		}
	}
}


/* Results:
The difference between the sum of the squares and the square of the sum of all numbers from 1-100 is 25164150
It took an average of 234.000 nanoseconds to run this problem through 100 iterations
*/
