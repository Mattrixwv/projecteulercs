//ProjectEuler/ProjectEulerCS/src/Problems/Problem5.cs
//Matthew Ellison
// Created: 08-23-20
//Modified: 07-05-21
//What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/CSClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


namespace ProjectEulerCS.Problems{
	public class Problem5 : Problem{
		//Variables
		//Instance variables
		private int smallestNum;	//The smallest number that is found
		public int Number{
			get{
				SolvedCheck("number");
				return smallestNum;
			}
		}

		//The results of the problem
		public override string Result{
			get{
				SolvedCheck("result");
				return $"The smallest positive number evenly divisible by all numbers 1-20 is {smallestNum}";
			}
		}

		//Functions
		//Constructor
		public Problem5() : base("What is the smallest positive number that is evenly divisible by all of the numbers from 1 to 20?"){
			smallestNum = 0;
		}
		//Operational functions
		//Solve the problem
		public override void Solve(){
			//If the problem has already been solved do nothing and end the function
			if(solved){
				return;
			}

			//Start the timer
			timer.Start();


			//Start at 20 because it must at least be divisible by 20. Increment by 2 because it must be an even number to be divisible by 2
			bool numFound = false;	//A flag for finding the divisible number
			int currentNum = 20;	//The number that it are currently checking against
			while((currentNum > 0) && (!numFound)){
				//Start by assuming you found the number (because we throw a flag if we didn't find it)
				numFound = true;
				//Step through every number from 1-20 seeing if the current number is divisible by it
				for(int divisor = 1;divisor <= 20;++divisor){
					//If it is not divisible then throw a flag and start looking at the next number
					if((currentNum % divisor) != 0){
						numFound = false;
						break;
					}
				}
				//If you didn't find the correct number then increment by 2
				if(!numFound){
					currentNum += 2;
				}
			}
			//Save the current num,ber as the smallest
			smallestNum = currentNum;


			//Stop the timer
			timer.Stop();

			//Throw a flag to show the problem is solved
			solved = true;
		}
		//Reset the problem so it can be run again
		public override void Reset(){
			base.Reset();
			smallestNum = 0;
		}
	}
}


/* Results:
The smallest positive number evenly divisible by all numbers 1-20 is 232792560
It took an average of 2.549 seconds to run this problem through 100 iterations
*/
