//ProjectEuler/ProjectEulerCS/src/Problems/Problem34.cs
//Matthew Ellison
// Created: 06-01-21
//Modified: 07-05-21
//Find the sum of all numbers which are equal to the sum of the factorial of their digits
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/CSClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


using System.Collections.Generic;


namespace ProjectEulerCS.Problems{
	public class Problem34 : Problem{
		//Variables
		//Static variables
		private static readonly int MAX_NUM = 1499999;	//The largest num that can be the sum of its own digits
		//Instance variables
		private readonly List<int> factorials;	//Holds the pre-computed factorials of the numbers 0-9
		private int sum;	//Holds the sum of all numbers equal to the sum of their digit's factorials
		//Gets
		//The results of the problem
		public override string Result{
			get{
				SolvedCheck("result");
				return $"The sum of all numbers that are the sum of their digit's factorials is {sum}";
			}
		}
		//Returns the list of factorials from 0-9
		public List<int> Factorials{
			get{
				SolvedCheck("list of factorials");
				return factorials;
			}
		}
		//Returns the sum of all numbers equal to the sum of their digit's factorials
		public int Sum{
			get{
				SolvedCheck("sum");
				return sum;
			}
		}

		//Functions
		//Constructor
		public Problem34() : base("Find the sum of all numbers which are equal to the sum of the factorial of their digits"){
			sum = 0;
			factorials = new List<int>(10);
			for(int cnt = 0;cnt <= 9; ++cnt){
				factorials.Add(0);
			}
		}
		//Operational functions
		//Solve the problem
		public override void Solve(){
			//If the problem has already been solved do nothing and end the function
			if(solved){
				return;
			}

			//Start the timer
			timer.Start();


			//Pre-compute the possible factorials from 0! to 9!
			for(int cnt = 0;cnt <= 9;++cnt){
				factorials[cnt] = mee.NumberAlgorithms.Factorial(cnt);
			}
			//Run through all possible numbers from 3-MAX_NUM and see if they equal the sum of their digit's factorials
			for(int cnt = 3;cnt < MAX_NUM;++cnt){
				//Split the number into its digits and add each one to the sum
				string numString = cnt.ToString();
				int currentSum = 0;
				foreach(char number in numString){
					int tempNum = (int)char.GetNumericValue(number);
					currentSum += factorials[tempNum];
				}
				//If the number is equal to the sum add the sum to the running sum
				if(currentSum == cnt){
					sum += currentSum;
				}
			}


			//Stop the timer
			timer.Stop();

			//Throw a flag to show the problem is solved
			solved = true;
		}
		//Reset the problem so it can be run again
		public override void Reset(){
			base.Reset();
			sum = 0;
			factorials.Clear();
			for(int cnt = 0;cnt <= 9;++cnt){
				factorials.Add(0);
			}
		}
	}
}


/* Results:
The sum of all numbers that are the sum of their digit's factorials is 40730
It took an average of 73.852 milliseconds to run this problem through 100 iterations
*/
