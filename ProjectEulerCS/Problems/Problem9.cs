//ProjectEuler/ProjectEulerCS/src/Problems/Problem9.cs
//Matthew Ellison
// Created: 08-23-20
//Modified: 07-05-21
//There exists exactly one Pythagorean triplet for which a + b + c = 1000. Find the product abc.
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/CSClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


using System;


namespace ProjectEulerCS{
	public class Problem9 : Problem{
		//Variables
		//Static varibles
		private const int GOAL_SUM = 1000;	//The number that we want the sum of a, b, and c to equal
		//Instance variables
		private int a;		//Holds the size of the first side
		public int SideA{
			get{
				SolvedCheck("first side");
				return a;
			}
		}
		private int b;		//Holds the size of the second side
		public int SideB{
			get{
				SolvedCheck("second side");
				return b;
			}
		}
		private double c;	//Holds the size of the hyp
		public int SideC{
			get{
				SolvedCheck("third side");
				return (int)c;
			}
		}
		private bool found;	//A flag to determine if we have found the solution yet
		public int Product{
			get{
				SolvedCheck("product of all three sides");
				return a * b * (int)c;
			}
		}

		//The results of the problem
		public override string Result{
			get{
				SolvedCheck("result");
				return $"The Pythagorean triplet is {a} + {b} + {Math.Round(c)}\nThe numbers' product is {a * b * Math.Round(c)}";
			}
		}
		//Functions
		//Constrcutor
		public Problem9() : base($"There exists exactly one Pythagorean triplet for which a + b + c = {GOAL_SUM}. Find the product abc."){
			a = 1;
			b = 0;
			c = 0;
			found = false;
		}
		//Operational functions
		//Solve the problem
		public override void Solve(){
			//If the problem has already been solved do nothing and end the function
			if(solved){
				return;
			}

			//Start the timer
			timer.Start();


			//Loop through all possible a's
			while((a < GOAL_SUM) && !found){
				b = a + 1;	//b must be larget than a
				c = Math.Sqrt((a * a) + (b * b));	//Compute the hyp

				//Loop through all possible b's for this a
				while((a + b + c) < GOAL_SUM){
					++b;
					c = Math.Sqrt((a * a) + (b * b));
				}

				//If the sum == 1000 you found the number, otherwise go to the next possible a
				if((a + b + c) == GOAL_SUM){
					found = true;
				}
				else{
					++a;
				}
			}


			//Stop the timer
			timer.Stop();

			//Throw a flag to show the problem is solved
			if(found){
				solved = true;
			}
			else{
				throw new Unsolved("THe problem was not solved!");
			}
		}
		//Reset the problem so it can be run again
		public override void Reset(){
			base.Reset();
			a = 1;
			b = 0;
			c = 0;
			found = false;
		}
	}
}


/* Results:
The Pythagorean triplet is 200 + 375 + 425
The numbers' product is 31875000
It took an average of 149.040 microseconds to run this problem through 100 iterations
*/
