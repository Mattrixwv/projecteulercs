//ProjectEuler/ProjectEulerCS/src/Problems/Problem16.cs
//Matthew Ellison
// Created: 08-25-20
//Modified: 08-27-20
//What is the sum of the digits of the number 2^1000?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/CSClasses
/*
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


using System;
using System.Numerics;


namespace ProjectEulerCS.Problems{
	public class Problem16 : Problem{
		//Variables
		//Static variables
		private const int NUM_TO_POWER = 2;	//The number that is going to be raised to a power
		private const int POWER = 1000;		//The power that the number is going to be raised to
		//Instance variables
		private BigInteger num;	//The number to be calculated
		public BigInteger Number{
			get{
				SolvedCheck("number");
				return num;
			}
		}
		private int sumOfElements;	//The sum of all digits in the number
		public int Sum{
			get{
				SolvedCheck("sum");
				return sumOfElements;
			}
		}

		//The results of the problem
		public override string Result{
			get{
				SolvedCheck("result");
				return $"{NUM_TO_POWER}^{POWER} = {num}\nThe sum of the elements is {sumOfElements}";
			}
		}

		//Functions
		//Constructor
		public Problem16() : base($"What is the sum of the digits of the number {NUM_TO_POWER}^{POWER}?"){
			num = 0;
			sumOfElements = 0;
		}
		//Operational functions
		//Solve the problem
		public override void Solve(){
			//If the problem has already been solved do nothing and end the function
			if(solved){
				return;
			}

			//Start the timer
			timer.Start();


			//Get the number
			num = BigInteger.Pow(NUM_TO_POWER, POWER);

			//Get a string of the number
			string numString = num.ToString();

			//Add up the individual characters of the string
			for(int cnt = 0;cnt < numString.Length;++cnt){
				sumOfElements += Convert.ToInt32(numString.Substring(cnt, 1));
			}


			//Stop the timer
			timer.Stop();

			//Throw a flag to show the problem is solved
			solved = true;
		}
		//Reset the problem so it can be run again
		public override void Reset(){
			base.Reset();
			num = 0;
			sumOfElements = 0;
		}
	}
}


/* Results:
2^1000 = 10715086071862673209484250490600018105614048117055336074437503883703510511249361224931983788156958581275946729175531468251871452856923140435984577574698574803934567774824230985421074605062371141877954182153046474983581941267398767559165543946077062914571196477686542167660429831652624386837205668069376
The sum of the elements is 1366
It took an average of 25.002 microseconds to run this problem through 100 iterations
*/
