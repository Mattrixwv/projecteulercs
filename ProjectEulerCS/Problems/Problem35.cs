//ProjectEuler/ProjectEulerCS/src/Problems/Problem35.cs
//Matthew Ellison
// Created: 06-05-21
//Modified: 07-05-21
//How many circular primes are there below one million?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/CSClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


using System.Collections.Generic;


namespace ProjectEulerCS.Problems{
	public class Problem35 : Problem{
		//Variables
		//Static variables
		private static readonly int MAX_NUM = 999999;	//The largest number that we are checking for primes
		//Instance variables
		private List<int> primes;	//The primes below MAX_NUM
		private readonly List<int> circularPrimes;	//The circular primes below MAX_NUM
		//Gets
		//The results of the problem
		public override string Result{
			get{
				SolvedCheck("result");
				return $"The number of all circular prime numbers under {MAX_NUM} is {circularPrimes.Count}";
			}
		}
		//Returns the vector of primes < MAX_NUM
		public List<int> Primes{
			get{
				SolvedCheck("result");
				return primes;
			}
		}
		//Returns the vector of circular primes < MAX_NUM
		public List<int> CircularPrimes{
			get{
				SolvedCheck("result");
				return circularPrimes;
			}
		}
		//Returns the number of circular primes
		public int NumCircularPrimes{
			get{
				SolvedCheck("number of circular primes");
				return circularPrimes.Count;
			}
		}

		//Functions
		//Returns a list of all rotations of a string passed to it
		private List<string> GetRotations(string str){
			List<string> rotations = new List<string>{str};
			for(int cnt = 1;cnt < str.Length;++cnt){
				str = str[1..] + str[0];
				rotations.Add(str);
			}
			return rotations;
		}
		//Constructor
		public Problem35() : base("How many circular primes are there below one million?"){
			primes = new List<int>();
			circularPrimes = new List<int>();
		}
		//Operational functions
		//Solve the problem
		public override void Solve(){
			//If the problem has already been solved do nothing and end the function
			if(solved){
				return;
			}

			//Start the timer
			timer.Start();


			//Get all primes under 1,000,000
			primes = mee.NumberAlgorithms.GetPrimes(MAX_NUM);
			//Go through all primes, get all their rotations, and check if those numbers are also primes
			foreach(int prime in primes){
				bool allRotationsPrime = true;
				//Get all of the rotations of the prime and see if they are also prime
				List<string> rotations = GetRotations(prime.ToString());
				foreach(string rotation in rotations){
					int p = int.Parse(rotation);
					if(!primes.Contains(p)){
						allRotationsPrime = false;
						break;
					}
				}
				//If all rotations are prime add it to the list of circular primes
				if(allRotationsPrime){
					circularPrimes.Add(prime);
				}
			}


			//Stop the timer
			timer.Stop();

			//Throw a flag to show the problem is solved
			solved = true;
		}
		//Reset the problem so it can be run again
		public override void Reset(){
			base.Reset();
			primes.Clear();
			circularPrimes.Clear();
		}
	}
}


/* Results:
The number of all circular prime numbers under 999999 is 55
It took an average of 1.946 seconds to run this problem through 100 iterations
*/
