//ProjectEuler/ProjectEulerCS/src/Problems/Problem37.cs
//Matthew Ellison
// Created: 06-30-21
//Modified: 06-30-21
//Find the sum of the only eleven primes that are both truncatable from left to right and right to left (2, 3, 5, and 7 are not counted).
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/CSClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


using System.Collections.Generic;


namespace ProjectEulerCS.Problems{
	public class Problem37 : Problem{
		//Variables
		//Static variables
		private readonly long LAST_PRIME_BEFORE_CHECK = 7;
		//Instance variables
		private readonly List<long> truncPrimes;	//All numbers that are truncatable primes
		private long sum;	//The sum of all elements in truncPrimes
		//Gets
		//The results of the problem
		public override string Result{
			get{
				SolvedCheck("result");
				return $"The sum of all left and right truncatable primes is {sum}";
			}
		}
		public List<long> TruncatablePrimes{
			get{
				SolvedCheck("list of truncatable primes");
				return truncPrimes;
			}
		}
		public long SumOfTruncatablePrimes{
			get{
				SolvedCheck("sum of truncatable primes");
				return sum;
			}
		}

		//Functions
		//Constructor
		public Problem37() : base("Find the sum of the only eleven primes that are both truncatable from left to right and right to left (2, 3, 5, and 7 are not counted)."){
			truncPrimes = new List<long>();
			sum = 0;
		}
		//Operational functions
		//Solve the problem
		public override void Solve(){
			//If the problem has already been solved do nothing and end the function
			if(solved){
				return;
			}

			//Start the timer
			timer.Start();


			//Create the sieve and get the first prime number
			IEnumerator<long> sieve = mee.NumberAlgorithms.SieveOfEratosthenes().GetEnumerator();
			sieve.MoveNext();
			//Loop through the sieve until you get to the LAST_PRIME_BEFORE_CHECK
			while(sieve.Current < LAST_PRIME_BEFORE_CHECK){
				sieve.MoveNext();
			}
			//Loop until trucPrimes contains 11 elements
			while(truncPrimes.Count < 11){
				bool isTruncPrime = true;
				//Get the next prime
				sieve.MoveNext();
				//Convert the prime to a string
				string primeString = sieve.Current.ToString();
				//If the string contains an even digit move to the next prime
				for(int strLoc = 0;(strLoc < primeString.Length) && (isTruncPrime);++strLoc){
					//Allow 2 to be the first digit
					if((strLoc == 0) && (primeString[strLoc] == '2')){
						continue;
					}
					switch(primeString[strLoc]){
						case '0' :
						case '2' :
						case '4' :
						case '6' :
						case '8' : isTruncPrime = false; break;
					}
				}
				//Start removing digits from the left and see if the number stays prime
				if(isTruncPrime){
					for(int truncLoc = 1;truncLoc < primeString.Length;++truncLoc){
						//Create a substring of the prime, removing the needed digits from the left
						string primeSubstring = primeString[truncLoc..];
						//Convert the string to an int and see if the number is still prime
						long newPrime = long.Parse(primeSubstring);
						if(!mee.NumberAlgorithms.IsPrime(newPrime)){
							isTruncPrime = false;
							break;
						}
					}
				}
				//Start removing digits from the right and see if the number stays prime
				if(isTruncPrime){
					for(int truncLoc = 1;truncLoc < primeString.Length;++truncLoc){
						//Create a substring of the prime, removing the needed digits from the right
						string primeSubstring = primeString.Substring(0, primeString.Length - truncLoc);
						//Conver the string to an int and see if the number is still prime
						long newPrime = long.Parse(primeSubstring);
						if(!mee.NumberAlgorithms.IsPrime(newPrime)){
							isTruncPrime = false;
							break;
						}
					}
				}
				//If the number remained prime through all operations add it to the vector
				if(isTruncPrime){
					truncPrimes.Add(sieve.Current);
				}
			}
			//Get the sum of all elements in the trucPrimes vector
			sum = mee.ArrayAlgorithms.GetSum(truncPrimes);


			//Stop the timer
			timer.Stop();

			//Throw a flag to show the problem is solved
			solved = true;
		}
		//Reset the porblem so it can be run again
		public override void Reset(){
			base.Reset();
			truncPrimes.Clear();
			sum = 0;
		}
	}
}


/* Results:
The sum of all left and right truncatable primes is 748317
It took an average of 82.833 milliseconds to run this problem through 100 iterations
*/
