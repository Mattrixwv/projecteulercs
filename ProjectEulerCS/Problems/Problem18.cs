//ProjectEuler/ProjectEulerCS/src/Problems/Problem18.cs
//Matthew Ellison
// Created: 08-25-20
//Modified: 08-27-20
//Find the maximum total from top to bottom
/*
75
95 64
17 47 82
18 35 87 10
20 04 82 47 65
19 01 23 75 03 34
88 02 77 73 07 63 67
99 65 04 28 06 16 70 92
41 41 26 56 83 40 80 70 33
41 48 72 33 47 32 37 16 94 29
53 71 44 65 25 43 91 52 97 51 14
70 11 33 28 77 73 17 78 39 68 17 57
91 71 52 38 17 14 91 43 58 50 27 29 48
63 66 04 68 89 53 67 30 73 16 69 87 40 31
04 62 98 27 23 09 70 98 73 93 38 53 60 04 23
*/
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/CSClasses
/*
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


using System;
using System.Collections.Generic;


namespace ProjectEulerCS.Problems{
	public class Problem18 : Problem{
		//Structures
		//Used to keep track of where the best location came from
		protected struct Location{
			public int Xlocation;
			public int Ylocation;
			public int Total;
			//Used originally for debugging so I could trace the path backwards
			public bool FromRight;
			public Location(int x, int y, int t, bool r){
				Xlocation = x;
				Ylocation = y;
				Total = t;
				FromRight = r;
			}
		}

		//The results of the problem
		public override string Result{
			get{
				SolvedCheck("result");
				return $"The value of the longest path is {actualTotal}";
			}
		}

		//Variables
		//Static variables
		//The list to hold the numbers in
		protected static List<List<int>> list;
		//Instance variables
		protected List<Location> foundPoints;	//For the points that I have already found the shortest distance to
		protected List<Location> possiblePoints;	//For the locations you are checking this round
		protected int actualTotal;	//The true total of the path from the top to the bottom
		public static string Pyramid{
			get{
				string results = "";
				//Loop through all elements of the list and print them
				foreach(List<int> row in list){
					foreach(int column in row){
						results += string.Format("%2d ", column);
					}
					results += '\n';
				}
				return results;
			}
		}
		public string Trail{
			get{
				SolvedCheck("trail of the shortest path");

				string results = "";
				//Save the trail the algorithm took
				List<Location> trail = new List<Location>{foundPoints[^1]};
				bool top = false;
				while(!top){
					bool found = false;
					int loc = foundPoints.Count - 1;
					Location? toAdd = null;
					while(!found){
						if(loc < 0){
							results += "Error: Location < 0\n";
							throw new Exception(results);
						}
						Location tempLoc = foundPoints[loc];
						if(trail[0].FromRight){
							if((tempLoc.Xlocation == trail[0].Xlocation) && (tempLoc.Ylocation == (trail[0].Ylocation - 1))){
								found = true;
								toAdd = tempLoc;
							}
							else{
								--loc;
							}
						}
						else{
							if((tempLoc.Xlocation == (trail[0].Xlocation - 1)) && (tempLoc.Ylocation == (trail[0].Ylocation - 1))){
								found = true;
								toAdd = tempLoc;
							}
							else{
								--loc;
							}
						}
					}

					trail.Insert(0, toAdd.Value);

					if(trail[0].Ylocation == 0){
						top = true;
					}
				}

				foreach(Location loc in trail){
					results += list[loc.Ylocation][loc.Xlocation];
					if(loc.Ylocation < (list.Count - 1)){
						results += "->";
					}
				}

				return results;
			}
		}
		public int Total{
			get{
				SolvedCheck("total");
				return actualTotal;
			}
		}

		//Functions
		//Static constructor
		static Problem18(){
			list = new List<List<int>>(){
				new List<int>{75},
				new List<int>{95, 64},
				new List<int>{17, 47, 82},
				new List<int>{18, 35, 87, 10},
				new List<int>{20, 04, 82, 47, 65},
				new List<int>{19, 01, 23, 75, 03, 34},
				new List<int>{88, 02, 77, 73, 07, 63, 67},
				new List<int>{99, 65, 04, 28, 06, 16, 70, 92},
				new List<int>{41, 41, 26, 56, 83, 40, 80, 70, 33},
				new List<int>{41, 48, 72, 33, 47, 32, 37, 16, 94, 29},
				new List<int>{53, 71, 44, 65, 25, 43, 91, 52, 97, 51, 14},
				new List<int>{70, 11, 33, 28, 77, 73, 17, 78, 39, 68, 17, 57},
				new List<int>{91, 71, 52, 38, 17, 14, 91, 43, 58, 50, 27, 29, 48},
				new List<int>{63, 66, 04, 68, 89, 53, 67, 30, 73, 16, 69, 87, 40, 31},
				new List<int>{04, 62, 98, 27, 23, 9, 70, 98, 73, 93, 38, 53, 60, 04, 23}
			};
		}
		//Constructor
		public Problem18() : base("Find the maximum total from top to bottom"){
			foundPoints = new List<Location>();
			possiblePoints = new List<Location>();
			actualTotal = 0;
		}
		//Operational functions
		//This function turns every number in the array into (100 - num) to allow you to find the largest numbers rather than the smallest
		protected void Invert(List<List<int>> list){
			//Loop through every row in the list
			for(int rowCnt = 0;rowCnt < list.Count;++rowCnt){
				//Loop through every column in the list
				for(int colCnt = 0;colCnt < list[rowCnt].Count;++colCnt){
					//The current element gets the value of 100 - value
					list[rowCnt][colCnt] = 100 - list[rowCnt][colCnt];
				}
			}
		}
		//This function helps by removing the element that is the same as the minimum location
		protected void RemoveHelper(List<Location> possiblePoints, Location minLocation){
			possiblePoints.RemoveAll(loc => ((loc.Xlocation == minLocation.Xlocation) && (loc.Ylocation == minLocation.Ylocation)));
		}
		//Operational functions
		//Solve the problem
		public override void Solve(){
			//If the problem has already been solved do nothing and end the function
			if(solved){
				return;
			}

			//Start the timer
			timer.Start();


			//Invert the list
			Invert(list);

			foundPoints.Add(new Location(0, 0, list[0][0], false));
			//Add the second row as possible points
			possiblePoints.Add(new Location(0, 1, (list[0][0] + list[1][0]), true));
			possiblePoints.Add(new Location(1, 1, (list[0][0] + list[1][1]), false));
			bool foundBottom = false;	//Used when you find a point at the bottom of the list

			//Loop until you find the bottom of the list
			while(!foundBottom){
				//Check which possible point gives us the lowest number. If more than one has the same number simply keep the first one
				Location minLoc = possiblePoints[0];
				foreach(Location loc in possiblePoints){
					if(loc.Total < minLoc.Total){
						minLoc = loc;
					}
				}

				//Remove it from the list of possible points
				RemoveHelper(possiblePoints, minLoc);

				//Add that point to the list of found points
				foundPoints.Add(minLoc);

				//Add to the list of possible points ifrom the point we just found and
				//If you are at the bottom raise the flag to end the program
				int xLoc = minLoc.Xlocation;
				int yLoc = minLoc.Ylocation + 1;	//Add one because you will always be moving to the next row
				if(yLoc >= list.Count){
					foundBottom = true;
				}
				else{
					possiblePoints.Add(new Location(xLoc, yLoc, minLoc.Total + list[yLoc][xLoc], true));
					++xLoc;	//Advance the x location to simulate going right
					//Check if x is out of bounds
					if(xLoc < list[yLoc].Count){
						possiblePoints.Add(new Location(xLoc, yLoc, minLoc.Total + list[yLoc][xLoc], false));
					}
				}
			}

			//Invert the list again so it is correct
			Invert(list);

			//Save the results
			//Get the correct total which will be the inversion of the current one
			actualTotal = ((100 * list.Count) - foundPoints[^1].Total);


			//Stop the timer
			timer.Stop();

			//Throw a flag to show the problem is solved
			solved = true;
		}
		//Reset the problem so it can be run again
		public override void Reset(){
			base.Reset();
			foundPoints.Clear();
			possiblePoints.Clear();
			actualTotal = 0;
		}
	}
}


/* Results:
The value of the longest path is 1074
It took an average of 32.856 microseconds to run this problem through 100 iterations
*/
