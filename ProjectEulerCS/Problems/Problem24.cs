//ProjectEuler/ProjectEulerCS/src/Problems/Problem24.cs
//Matthew Ellison
// Created: 09-03-20
//Modified: 07-05-21
//What is the millionth lexicographic permutation of the digits 0, 1, 2, 3, 4, 5, 6, 7, 8 and 9?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/CSClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


using System.Collections.Generic;


namespace ProjectEulerCS.Problems{
	public class Problem24 : Problem{
		//Variables
		//Static variables
		private const int NEEDED_PERM = 1000000;	//The number of the permutation that you need
		private const string nums = "0123456789";	//All of the characters that we need to get the permutations of
		//Instance variables
		private List<string> permutations;	//Holds all of the permutations of the string nums
		public List<string> PermutationsList{
			get{
				SolvedCheck("permutations");
				return permutations;
			}
		}
		public string Permutation{
			get{
				SolvedCheck("1,000,000th permutation");
				return permutations[NEEDED_PERM - 1];
			}
		}
		public override string Result{
			get{
				SolvedCheck("result");
				return $"The 1 millionth permutation is {Permutation}";
			}
		}

		//Functions
		//Constructor
		public Problem24() : base($"What is the millionth lexicographic permutation of the digits {nums}?"){
			permutations = new List<string>();
		}
		//Operational functions
		//Solved the problem
		public override void Solve(){
			//If the problem has already been solved do nothing and end the function
			if(solved){
				return;
			}

			//Start the timer
			timer.Start();


			//Get all the permutations of the string
			permutations = mee.StringAlgorithms.GetPermutations(nums);


			//Stop the timer
			timer.Stop();

			//Throw a flag to show the problem is solved
			solved = true;
		}
		//Reset the problem so it can be run again
		public override void Reset(){
			base.Reset();
			permutations.Clear();
		}
	}
}


/* Results:
The 1 millionth permutation is 2783915460
It took an average of 5.865 seconds to run this problem through 100 iterations
*/
