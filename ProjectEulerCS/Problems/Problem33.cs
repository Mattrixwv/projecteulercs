//ProjectEuler/ProjectEulerCS/src/Problems/Problem33.cs
//Matthew Ellison
// Created: 02-06-21
//Modified: 07-05-21
/*
The fraction 49/98 is a curious fraction, as an inexperienced mathematician in attempting to simplify it may incorrectly believe that 49/98 = 4/8, which is correct, is obtained by cancelling the 9s
We shall consider fractions like, 30/50 = 3/5, to be trivial examples
There are exactly four non-trivial examples of this type of fraction, less than one in value, and containing two digits in the numerator and denominator
If the product of these four fractions is given in its lowest common terms, find the value of the denominator
*/
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/CSClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


using System.Collections.Generic;


namespace ProjectEulerCS.Problems{
	public class Problem33 : Problem{
		//Variables
		//Static variables
		private static readonly int MIN_NUMERATOR = 10;		//The lowest the numerator can be
		private static readonly int MAX_NUMERATOR = 98;		//The highest the numerator can be
		private static readonly int MIN_DENOMINATOR = 11;	//The lowest the denominator can be
		private static readonly int MAX_DENOMINATOR = 99;	//The highest the denominator can be
		//Instance variables
		private readonly List<int> numerators;		//Holds the numerators that were found
		private readonly List<int> denominators;	//Holds the denominators that were found
		private int prodDenominator;				//Holds the answer to question
		//Gets
		//The results of the problem
		public override string Result{
			get{
				SolvedCheck("result");
				return $"The denominator of the product is {prodDenominator}";
			}
		}
		public List<int> Numerators{
			get{
				SolvedCheck("list of numerators");
				return numerators;
			}
		}
		public List<int> Denominators{
			get{
				SolvedCheck("list of denominators");
				return denominators;
			}
		}
		public int ProdDenominator{
			get{
				SolvedCheck("result");
				return prodDenominator;
			}
		}

		//Functions
		//Constructor
		public Problem33() : base("If the product of these four fractions is given in its lowest common terms, find the value of the denominator"){
			numerators = new List<int>();
			denominators = new List<int>();
			prodDenominator = 1;
		}
		//Operational functions
		//Solve the problem
		public override void Solve(){
			//If the problem has already been solved do nothing and end the function
			if (solved){
				return;
			}

			//Start the timer
			timer.Start();


			//Search every possible numerator/denominator pair
			for(int denominator = MIN_DENOMINATOR;denominator <= MAX_DENOMINATOR;++denominator){
				for(int numerator = MIN_NUMERATOR;(numerator < denominator) &&(numerator <= MAX_NUMERATOR);++numerator){
					string denom = denominator.ToString();
					string num = numerator.ToString();
					int tempNum = 0;
					int tempDenom = 1;

					//Check that this isn't a trivial example
					if((num[1] == '0') && (denom[1] == '0')){
						continue;
					}
					//Remove the offending digits if they exist
					else if(num[0] == denom[0]){
						tempNum = num[1] - 48;
						tempDenom = denom[1] - 48;
					}
					else if(num[0] == denom[1]){
						tempNum = num[1] - 48;
						tempDenom = denom[0] - 48;
					}
					else if(num[1] == denom[0]){
						tempNum = num[0] - 48;
						tempDenom = denom[1] - 48;
					}
					else if(num[1] == denom[1]){
						tempNum = num[0] - 48;
						tempDenom = denom[0] - 48;
					}

					//Test if the new fraction is the same as the old one
					if(((double)tempNum / tempDenom) == ((double)numerator / denominator)){
						numerators.Add(numerator);
						denominators.Add(denominator);
					}
				}
			}

			//Get the product of the numbers
			int numProd = mee.ArrayAlgorithms.GetProd(numerators);
			int denomProd = mee.ArrayAlgorithms.GetProd(denominators);
			//Get the gcd to reduce to lowest terms
			int gcd = mee.NumberAlgorithms.GCD(numProd, denomProd);
			//Save the denominator
			prodDenominator = denomProd / gcd;


			//Stop the timer
			timer.Stop();

			//Thow a flag to show the problem is solved
			solved = true;
		}
		//Reset the problem so it can be run again
		public override void Reset(){
			base.Reset();
			numerators.Clear();
			denominators.Clear();
		}
	}
}


/* Results:
The denominator of the product is 100
It took an average of 221.559 microseconds to run this problem through 100 iterations
*/
