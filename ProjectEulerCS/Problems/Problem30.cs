//ProjectEuler/ProjectEulerCS/src/Problems/Problem30.cs
//Matthew Ellison
// Created: 10-03-20
//Modified: 07-05-21
//Find the sum of all the numbers that can be written as the sum of the fifth powers of their digits.
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/CSClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


using System;
using System.Collections.Generic;


namespace ProjectEulerCS.Problems{
	public class Problem30 : Problem{
		//Variables
		//Static variables
		private const long TOP_NUM = 1000000;	//This is the largest number that will be checked
		private const long BOTTOM_NUM = 2;		//Starts with 2 because 0 and 1 don't count
		private const long POWER_RAISED = 5;	//Starts with 2 because 0 and 1 don't count
		//Instance variables
		private readonly List<long> SumOfFifthNumbers;	//This is an ArrayList of the numbers that are the sum of the fifth power of their digits
		private long sum;	//This is the sum of the sumOfFifthNumbers array
		//Gets
		public long TopNum{
			get{
				SolvedCheck("largest number checked");
				return TOP_NUM;
			}
		}
		public List<long> ListOfSumOfFifths{
			get{
				SolvedCheck("list of all numbers that are the sum of the 5th power of their digits");
				return SumOfFifthNumbers;
			}
		}
		public long GetSumOfList{
			get{
				SolvedCheck("sum of all numbers that are the sum of the 5th power of their digits");
				return sum;
			}
		}
		public override string Result{
			get{
				SolvedCheck("result");
				return $"The sum of all the numbers that can be written as the sum of the fifth powers of their digits is {sum}";
			}
		}

		//Functions
		//Operational functions
		public Problem30() : base("Find the sum of all the numbers that can be written as the sum of the fifth powers of their digits."){
			SumOfFifthNumbers = new List<long>();
			sum = 0;
		}
		//Operational functions
		//Returns an ArrayList with the individual digits of the number passed to it
		private List<long> GetDigits(long num){
			List<long> listOfDigits = new List<long>();	//This ArrayList holds the individual digits of num
			//The easiest way to get the individual digits of a number is by converting it to a string
			string digits = num.ToString();
			//Start with the first digit, convert it to an integer, store it in the ArrayList, and move to the next digit
			for(int cnt = 0;cnt < digits.Length;++cnt){
				listOfDigits.Add((long)char.GetNumericValue(digits[cnt]));
			}
			//Return the list of digits
			return listOfDigits;
		}
		//Solve the problem
		public override void Solve(){
			//If the problem has already been solved do nothing and end the function
			if(solved){
				return;
			}

			//Start the timer
			timer.Start();


			//Start with the lowest number and increment until you reach the largest number
			for(long currentNum = BOTTOM_NUM;currentNum <= TOP_NUM;++currentNum){
				//Get the digits of the number
				List<long> digits = GetDigits(currentNum);
				//Get the sum of the powers
				long sumOfPowers = 0;
				foreach(long num in digits){
					sumOfPowers += (long)Math.Round(Math.Pow(num, POWER_RAISED));
				}
				//Check if the sum of the powers is the same as the number
				//If it is, add it tot he list, owtherwise continue to the next number
				if(sumOfPowers == currentNum){
					SumOfFifthNumbers.Add(currentNum);
				}
			}

			sum = mee.ArrayAlgorithms.GetSum(SumOfFifthNumbers);


			//Stop the timer
			timer.Stop();

			//Throw a flag to show the problem is solved
			solved = true;
		}
		//Reset the problem so it can be run again
		public override void Reset(){
			base.Reset();
			SumOfFifthNumbers.Clear();
			sum = 0;
		}
	}
}


/* Results:
The sum of all the numbers that can be written as the sum of the fifth powers of their digits is 443839
It took an average of 217.218 milliseconds to run this problem through 100 iterations
*/
