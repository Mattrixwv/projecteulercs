//ProjectEuler/ProjectEulerCS/src/Problems/Problem3.cs
//Matthew Ellison
// Created: 08-23-20
//Modified: 07-05-21
//The largest prime factor of 600851475143
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/CSClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


using System.Collections.Generic;


namespace ProjectEulerCS.Problems{
	public class Problem3 : Problem{
		//Variables
		//Static variables
		private const long GOAL_NUMBER = 600851475143L;	//The number that needs factored
		public static long GoalNumber{
			get{ return GOAL_NUMBER; }
		}
		//Instance variables
		private List<long> factors;	//Holds the factors of goalNumber
		public List<long> Factors{
			get{
				SolvedCheck("factors");
				return factors;
			}
		}
		public long LargestFactor{
			get{
				SolvedCheck("largest factor");
				return factors[^1];
			}
		}

		//The results of the problem
		public override string Result{
			get{
				SolvedCheck("result");
				return $"The largest factor of the number {GOAL_NUMBER} is {factors[^1]}";
			}
		}
		//Functions
		//Constructor
		public Problem3() : base($"What is the largest prime factor of {GOAL_NUMBER}?"){
			factors = new List<long>();
		}
		//Operational functions
		//Solve the problem
		public override void Solve(){
			//If the problem has already been solved do nothing and end the function
			if(solved){
				return;
			}

			//Star the timer
			timer.Start();


			//Get all the factors of the number
			factors = mee.NumberAlgorithms.GetFactors(GOAL_NUMBER);
			//THe last element should be the largest factor


			//Stop the timer
			timer.Stop();

			//Throw a flag to show the problem is solved
			solved = true;
		}
		//Reset the porblem so it can be run again
		public override void Reset(){
			base.Reset();
			factors.Clear();
		}
	}
}


/*Results:
The largest factor of the number 600851475143 is 6857
It took an average of 47.412 milliseconds to run this problem through 100 iterations
*/
