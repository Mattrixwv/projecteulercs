//ProjectEuler/ProjectEulerCS/src/Problems/Problem23.cs
//Matthew Ellison
// Created: 09-03-20
//Modified: 07-05-21
//Find the sum of all the positive integers which cannot be written as the sum of two abundant numbers
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/CSClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


using System.Collections.Generic;


namespace ProjectEulerCS.Problems{
	public class Problem23 : Problem{
		//Variables
		//Static variables
		private const int MAX_NUM = 28123;	//THe largest number to be checked
		//Instance variables
		private readonly List<int> divisorSums;	//This gives the sum of the divisors at subscripts
		private long sum;	//The sum of all the numbers we are looking for
		public override string Result{
			get{
				SolvedCheck("result");
				return $"The answer is {sum}";
			}
		}
		public long Sum{
			get{
				SolvedCheck("sum");
				return sum;
			}
		}

		//Functions
		//Constructor
		public Problem23() : base("Find the sum of all the positive integers which cannot be written as the sum of two abundant numbers"){
			divisorSums = new List<int>();
			ReserveArray();
			sum = 0;
		}
		//Operational functions
		//Reserve the size of the array to speed up insertion
		private void ReserveArray(){
			divisorSums.Capacity = MAX_NUM;	//It is faster to reserve the appropriate amount of ram now
			//Make sure every element has a 0 in it's location
			while(divisorSums.Count <= MAX_NUM){
				divisorSums.Add(0);
			}
		}
		//Solve the problem
		public override void Solve(){
			//If the problem has already been solved do nothing and end the function
			if(solved){
				return;
			}

			//Start the timer
			timer.Start();


			//Get the sum of the divisors of all numbers < MAX_NUM
			for(int cnt = 1;cnt < MAX_NUM;++cnt){
				List<int> div = mee.NumberAlgorithms.GetDivisors(cnt);
				//Remove the last element, which is the number itself. This gives us the propper divisors
				if(div.Count > 1){
					div.Remove(div[^1]);
				}
				divisorSums[cnt] = mee.ArrayAlgorithms.GetSum(div);
			}

			//Get the abundant numbers
			List<int> abund = new List<int>();
			for(int cnt = 0;cnt < divisorSums.Count;++cnt){
				if(divisorSums[cnt] > cnt){
					abund.Add(cnt);
				}
			}

			//Check if each number can be the sum of 2 abundant numbers and add to the sum if no
			for(int cnt = 1;cnt < MAX_NUM;++cnt){
				if(!IsSum(abund, cnt)){
					sum += cnt;
				}
			}


			//Stop the timer
			timer.Stop();

			//Throw a flag to show the problem is solved
			solved = true;
		}
		//A function that returns true if num can be created by adding two elements from abund and false if it cannot
		private bool IsSum(List<int> abund, int num){
			int sum;
			//Pick a number for the first part of the sum
			for(int firstNum = 0;firstNum < abund.Count;++firstNum){
				//Pick a number for the second part of the sum
				for(int secondNum = firstNum;secondNum < abund.Count;++secondNum){
					sum = abund[firstNum] + abund[secondNum];
					if(sum == num){
						return true;
					}
					else if(sum > num){
						break;
					}
				}
			}
			//If you have run through the entire list and did not find a sum then it is false
			return false;
		}
		//Reset the problem so it can be run again
		public override void Reset(){
			base.Reset();
			divisorSums.Clear();
			ReserveArray();
			sum = 0;
		}
	}
}


/* Results:
The answer is 4179871
It took an average of 11.420 seconds to run this problem through 100 iterations
*/
