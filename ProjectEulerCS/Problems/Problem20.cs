//ProjectEuler/ProjectEulerCS/src/Problems/Problem20.cs
//Matthew Ellison
// Created: 08-27-20
//Modified: 07-05-21
//What is the sum of the digits of 100!?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/CSClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


using System.Numerics;


namespace ProjectEulerCS.Problems{
	public class Problem20 : Problem{
		//Variables
		//Static variables
		private const int TOP_NUM = 100;	//The largest number that will be multiplied
		//Instance variables
		private BigInteger num;	//Holds the number 100!
		public BigInteger Number{
			get{
				SolvedCheck("number");
				return num;
			}
		}
		public string NumberString{
			get{
				SolvedCheck("number as a string");
				return num.ToString();
			}
		}
		private long sum;	//The sum of the digits of num
		public long Sum{
			get{
				SolvedCheck("sum of the digits");
				return sum;
			}
		}

		//The results of the problem
		public override string Result{
			get{
				SolvedCheck("result");
				return $"{TOP_NUM}! = {num}\nThe sum of the digits is: {sum}";
			}
		}

		//Functions
		//Constructor
		public Problem20() : base($"What is the sum of the digits of {TOP_NUM}!?"){
			num = 1;
			sum = 0;
		}
		//Operational functions
		//Solve the problem
		public override void Solve(){
			//If the problem has already been solved do nothing and end the function
			if(solved){
				return;
			}

			//Start the timer
			timer.Start();


			//Run through every number from 1 to 100 and multiply it by the current num to generate 100!
			for(int cnt = TOP_NUM;cnt > 1;--cnt){
				num *= cnt;
			}

			//Get a string of the number because it is easier to pull apart the individual characters 
			string numString = num.ToString();
			//Run through every character in the string, convert it back to an integer and add it to the running sum
			for(int cnt = 0;cnt < numString.Length;++cnt){
				char digit = numString[cnt];
				sum += (long)char.GetNumericValue(digit);
			}


			//Stop the timer
			timer.Stop();

			//Throw a flag to show the problem is solved
			solved = true;
		}
		//Reset the problem so it can be run again
		public override void Reset(){
			base.Reset();
			num = 1;
			sum = 0;
		}
	}
}


/* Results:
100! = 93326215443944152681699238856266700490715968264381621468592963895217599993229915608941463976156518286253697920827223758251185210916864000000000000000000000000
The sum of the digits is: 648
It took an average of 11.740 microseconds to run this problem through 100 iterations
*/
