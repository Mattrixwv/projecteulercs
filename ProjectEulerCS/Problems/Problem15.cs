//ProjectEuler/ProjectEulerCS/src/Problems/Problem15.cs
//Matthew Ellison
// Created: 08-25-20
//Modified: 07-05-21
//How many routes from the top left corner to the bottom right corner are there through a 20×20 grid if you can only move right and down?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/CSClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


namespace ProjectEulerCS.Problems{
	public class Problem15 : Problem{
		//Variables
		//Static variables
		private const int WIDTH = 20;	//The width of the box to traverse
		private const int LENGTH = 20;	//The height of the box to traverse
		//Instance variables
		private long numOfRoutes;	//The number of routes from 0, 0 to 20, 20
		public long NumberOfRoutes{
			get{
				SolvedCheck("number of routes");
				return numOfRoutes;
			}
		}

		//The results of the problem
		public override string Result{
			get{
				SolvedCheck("result");
				return $"The number of routes is {numOfRoutes}";
			}
		}

		//Functions
		//Constructor
		public Problem15() : base("How many routes from the top left corner to the bottom right corner are there through a 20x20 grid if you can only move right and down?"){
			numOfRoutes = 0;
		}
		//Operational functions
		//Solve the problems
		public override void Solve(){
			//If the problem has already been solved do nothing and end the function
			if(solved){
				return;
			}

			//Start the timer
			timer.Start();


			//We write this as a recursive function
			//When in a location it always moves right first, then down
			Move(0, 0);


			//Stop the timer
			timer.Stop();

			//Throw a flag to show the problem is solved
			solved = true;
		}
		//This function acts as a handler for moving the position on the grid and counting the distance
		//It moves right first, then down
		private void Move(int currentX, int currentY){
			//Check if you are at the end and act accordingly
			if((currentX == WIDTH) && (currentY == LENGTH)){
				++numOfRoutes;
				return;
			}

			//Move right if possible
			if(currentX < WIDTH){
				Move(currentX + 1, currentY);
			}

			//Move down if possible
			if(currentY < LENGTH){
				Move(currentX, currentY + 1);
			}
		}
		//Reset the problem so it can be run again
		public override void Reset(){
			base.Reset();
			numOfRoutes = 0;
		}
	}
}


/* Results
The number of routes is 137846528820
It took 17.328 minutes to solve this problem.
*/
