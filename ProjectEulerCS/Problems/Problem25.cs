//ProjectEuler/ProjectEulerCS/src/Problems/Problem25.cs
//Matthew Ellison
// Created: 09-11-20
//Modified: 07-05-21
//What is the index of the first term in the Fibonacci sequence to contain 1000 digits?
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/CSClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


using System.Numerics;


namespace ProjectEulerCS.Problems{
	public class Problem25 : Problem{
		//Variables
		//Static variables
		private const int NUM_DIGITS = 1000;	//The number of digits to calculate up to
		//Instance variables
		private BigInteger number;	//The current Fibonacci number
		private BigInteger index;	//The index of the current Fibonacci number just calculated
		public BigInteger Number{
			get{
				SolvedCheck("fibonacci number");
				return number;
			}
		}
		public BigInteger Index{
			get{
				SolvedCheck("index of the fibonacci number");
				return index;
			}
		}
		public override string Result{
			get{
				SolvedCheck("result");
				return $"The first Fibonacci number with {NUM_DIGITS} digits is {number}\nIts index is {index}";
			}
		}

		//Functions
		//Constructor
		public Problem25() : base($"What is the index of the first term in the Fibonacci sequence to contain {NUM_DIGITS} digits?"){
			number = 0;
			index = 2;
		}
		//Operational functions
		//Solve the problem
		public override void Solve(){
			//If the problem has already been solved do nothing and end the function
			if(solved){
				return;
			}

			//Star the timer
			timer.Start();


			//Move through all Fibonacci numbers until you reach the one with at least NUM_DIGITS digits
			while(number.ToString().Length < NUM_DIGITS){
				index += 1;	//Increase the index number. Doing this at the beginning keeps the index correct at the end of the loop
				number = mee.NumberAlgorithms.GetFib(index);	//Calculate the number
			}


			//Stop the timer
			timer.Stop();

			//THrow a flag to show the porblem is solved
			solved = true;
		}
		//Reset the problem so it can be run again
		public override void Reset(){
			base.Reset();
			number = 0;
			index = 2;
		}
	}
}


/* Results:
The first Fibonacci number with 1000 digits is 1070066266382758936764980584457396885083683896632151665013235203375314520604694040621889147582489792657804694888177591957484336466672569959512996030461262748092482186144069433051234774442750273781753087579391666192149259186759553966422837148943113074699503439547001985432609723067290192870526447243726117715821825548491120525013201478612965931381792235559657452039506137551467837543229119602129934048260706175397706847068202895486902666185435124521900369480641357447470911707619766945691070098024393439617474103736912503231365532164773697023167755051595173518460579954919410967778373229665796581646513903488154256310184224190259846088000110186255550245493937113651657039447629584714548523425950428582425306083544435428212611008992863795048006894330309773217834864543113205765659868456288616808718693835297350643986297640660000723562917905207051164077614812491885830945940566688339109350944456576357666151619317753792891661581327159616877487983821820492520348473874384736771934512787029218636250627816
Its index is 4782
It took an average of 1.008 seconds to run this problem through 100 iterations
*/
