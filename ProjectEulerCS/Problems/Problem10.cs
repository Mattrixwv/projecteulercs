//ProjectEuler/ProjectEulerCS/src/Problems/Problem10.cs
//Matthew Ellison
// Created: 08-23-20
//Modified: 07-05-21
//Find the sum of all the primes below two million
//Unless otherwise listed all non-standard includes are my own creation and available from https://bibucket.org/Mattrixwv/CSClasses
/*
	Copyright (C) 2021  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


using System.Linq;


namespace ProjectEulerCS{
	public class Problem10 : Problem{
		//Variables
		//Static variables
		private const long GOAL_NUMBER = 2000000 - 1;
		//Instance variables
		private long sum;	//The sum of all the prime numbers
		public long Sum{
			get{
				SolvedCheck("sum");
				return sum;
			}
		}

		//The results of the problem
		public override string Result{
			get{
				SolvedCheck("result");
				return $"The sum of all the primes < {GOAL_NUMBER + 1} is {sum}";
			}
		}

		//Functions
		//Constructor
		public Problem10() : base($"Find the sum of all the primes below {GOAL_NUMBER + 1}."){
			sum = 0;
		}
		//Operational functions
		//Solve the problem
		public override void Solve(){
			//If the problem has already been solved do nothing and end the function
			if(solved){
				return;
			}

			//Start the timer
			timer.Start();


			//Get the sum of all prime numbers < GOAL_NUMBER
			sum = mee.NumberAlgorithms.GetPrimes(GOAL_NUMBER).Sum();


			//Stop the timer
			timer.Stop();

			//Throw a flag to show the problem is solved
			solved = true;
		}
		//Reset the problem so it can be run again
		public override void Reset(){
			base.Reset();
			sum = 0;
		}
	}
}


/* Results:
The sum of all the primes < 2000000 is 142913828922
It took an average of 165.413 milliseconds to run this problem through 100 iterations
*/
