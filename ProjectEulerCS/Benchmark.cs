﻿//ProjectEuler/ProjectEulerCS/src/ProblemSelection.cs
//Matthew Ellison
// Created: 08-22-20
//Modified: 09-04-20
//This runs the benchmark functions for the Java version of the ProjectEuler project
/*
	Copyright (C) 2020  Matthew Ellison

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU Lesser General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Lesser General Public License for more details.

	You should have received a copy of the GNU Lesser General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/


using System.Collections.Generic;


namespace ProjectEulerCS{
	public class Benchmark{
		private enum BenchmarkOptions { RunSpecific = 1, RunAllShort, RunAll, Exit, Size };
		private static readonly List<int> tooLong = new List<int>()
				{5, 15, 23, 24, 35};

		//The driver function for the benchmark selection
		public static void BenchmarkMenu(){
			BenchmarkOptions selection;

			PrintMenu();
			selection = GetMenuSelection();

			switch(selection){
				case BenchmarkOptions.RunSpecific: RunSpecific(); break;
				case BenchmarkOptions.RunAllShort: RunAllShort(); break;
				case BenchmarkOptions.RunAll: RunAll(); break;
				case BenchmarkOptions.Exit: break;
				case BenchmarkOptions.Size: break;
			}
		}
		//Print the benchmark menu
		private static void PrintMenu(){
			System.Console.WriteLine("1. Run a specific problem");
			System.Console.WriteLine("2. Run all problems that have a reasonably short run time");
			System.Console.WriteLine("3. Run all problems");
			System.Console.WriteLine("4. Exit the menu");
			System.Console.WriteLine();
		}
		//Returns a valid menu option
		private static BenchmarkOptions GetMenuSelection(){
			string selectionString = System.Console.ReadLine();
			int selection = System.Convert.ToInt32(selectionString);
			while(!IsValidMenu(selection)){
				System.Console.WriteLine("That is an invalid option!\nPress Enter to continue");
				PrintMenu();
				selectionString = System.Console.ReadLine();
				selection = System.Convert.ToInt32(selectionString);
			}
			return GetSelection(selection);
		}
		//Determines if a value is a valid menu option. Helper for getBenchmarMenuSeleciton
		private static bool IsValidMenu(int selection){
			if((selection >= (int)BenchmarkOptions.RunSpecific) && (selection < (int)BenchmarkOptions.Size)){
				return true;
			}
			else{
				return false;
			}
		}
		//A helper function for getMenuSelection that turns an integer to a BenchmarkOptions
		private static BenchmarkOptions GetSelection(int selection){
			BenchmarkOptions sel = selection switch{
				1 => BenchmarkOptions.RunSpecific,
				2 => BenchmarkOptions.RunAllShort,
				3 => BenchmarkOptions.RunAll,
				4 => BenchmarkOptions.Exit,
				_ => BenchmarkOptions.Size,
			};
			return sel;
		}
		//Determines which problem user wants to run and runs it
		private static void RunSpecific(){
			//Ask which problem the user wants to run
			int problemNumber = ProblemSelection.GetProblemNumber();
			//Ask how many times to run the problem
			int timesToRun = GetNumberOfTimesToRun();

			//Get the problem and print its description
			Problem problem = ProblemSelection.GetProblem(problemNumber);
			System.Console.WriteLine(problemNumber + ". " + problem.Description);

			//Run the problem the specific number of times
			decimal totalTime = RunProblem(problem, timesToRun);

			//Print the results
			System.Console.WriteLine(GetBenchmarkResults(problem, totalTime, timesToRun));
		}
		//Runs all problems except a few that are specified because of run length
		private static void RunAllShort(){
			//Ask how many times to run the problems
			int timesToRun = GetNumberOfTimesToRun();

			//Run through all valid problem numbers, skipping a few that are in the tooLong list
			for(int cnt = 1; cnt < ProblemSelection.PROBLEM_NUMBERS.Count; ++cnt){
				int problemNumber = ProblemSelection.PROBLEM_NUMBERS[cnt];

				//If the problem number is contained in the list of problems that take too long skip it
				if(tooLong.IndexOf(problemNumber) != -1){
					continue;
				}

				//Get the problem and print its description
				Problem problem = ProblemSelection.GetProblem(problemNumber);
				System.Console.WriteLine(problemNumber + ". " + problem.Description);

				//Run each problem the specified number of times
				decimal totalTime = RunProblem(problem, timesToRun);

				//Print the results
				System.Console.WriteLine(GetBenchmarkResults(problem, totalTime, timesToRun));
			}
		}
		//Runs all problems
		private static void RunAll(){
			//Ask how many times to run the problem
			int timesToRun = GetNumberOfTimesToRun();

			//Run through all valid problem numbers, skipping a few that are in the tooLong list
			for(int cnt = 1; cnt < ProblemSelection.PROBLEM_NUMBERS.Count; ++cnt){
				int problemNumber = ProblemSelection.PROBLEM_NUMBERS[cnt];

				//Get the problem
				Problem problem = ProblemSelection.GetProblem(problemNumber);

				//Run each problem the specified number of times
				System.Console.WriteLine(problemNumber + ". " + problem.Description);
				decimal totalTime = RunProblem(problem, timesToRun);

				//Print the results
				System.Console.WriteLine(GetBenchmarkResults(problem, totalTime, timesToRun));
			}
		}
		//Asks how many times a problem is supposed to run and returns the value
		private static int GetNumberOfTimesToRun(){
			int numOfTimesToRun;
			System.Console.Write("How many times do you want to run this problem? ");
			string numOfTimesToRunString = System.Console.ReadLine();
			numOfTimesToRun = System.Convert.ToInt32(numOfTimesToRunString);
			while(numOfTimesToRun < 1){
				System.Console.WriteLine("That is an invalid number!");
				System.Console.Write("How many times do you want to run this problem? ");
				numOfTimesToRunString = System.Console.ReadLine();
				numOfTimesToRun = System.Convert.ToInt32(numOfTimesToRunString);
			}
			return numOfTimesToRun;
		}
		//Runs the problem the given number of times
		private static decimal RunProblem(Problem problem, int timesToRun){
			decimal totalTime = 0;
			System.Console.Write("Solving");
			for(int cnt = 0; cnt < timesToRun; ++cnt){
				System.Console.Write('.');
				//Reset the data so you care actually counting the run time an additional time
				problem.Reset();
				//Solve the problem
				problem.Solve();
				//Get the time data
				totalTime += problem.Timer.GetNano();
			}
			return totalTime;
		}
		//Prints the benchmark results of a problem
		private static string GetBenchmarkResults(Problem problem, decimal totalTime, int timesRun){
			//Calculate the average run time of the problem
			totalTime /= timesRun;
			string timeResults = mee.Stopwatch.GetStr(totalTime);

			//Tally the results
			string results = "\n\n" + problem.Result + "\nIt took an average of " + timeResults + " to run this problem through " + timesRun + " iterations\n\n";
			return results;
		}
	}
}